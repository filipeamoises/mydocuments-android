package br.com.moises.mydocs.rest.login

import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.Login
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.rest.BaseRestService
import br.com.moises.mydocs.rest.JsonRestReponseCallback

/**
 * Created by Filipe on 12/04/2018.
 */
open class LoginRestService(private val login: Login, callback: JsonRestReponseCallback<Auth>) : BaseRestService<Login, Auth, UserRestApi>(callback) {

    override val apiClass: Class<UserRestApi>
        get() = UserRestApi::class.java

    override fun create(): Int = 0

    public override fun execute(oauthRestApi: UserRestApi) {
        super.enqueue(oauthRestApi.login(login))
    }

    public override fun onRestSuccess(tokenResponse: Auth?) {
        super.onRestSuccess(tokenResponse)
    }

    public override fun onRestError(response: ProblemApiResponse) {
        super.onRestError(response)
    }

    public override fun onFailure(t: Throwable) {
        super.onFailure(t)
    }
}