package br.com.moises.mydocs.dao.image

/**
 * Created by Filipe on 10/04/2018.
 */
object ImageProvider {

    private val dao = ImageDAO.instance

    @Synchronized
    fun hasFile(fileUri: String): Boolean = dao.hasFile(fileUri)

    @Synchronized
    fun find(fileUri: String): ImageModel? = dao.find(fileUri)

    @Synchronized
    fun setUploaded(fileId: Long) = dao.setUploaded(fileId)

    @Synchronized
    fun delete(fileUri: String) = dao.delete(fileUri)

    @Synchronized
    fun save(file: ImageModel) = dao.save(file)

    @Synchronized
    fun deleteAllByUserId(userId: Long) = dao.deleteAllByUserId(userId)

    @Synchronized
    fun findAllByUserId(userId: Long) = dao.findImageByUserId(userId)
}