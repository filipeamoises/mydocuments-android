package br.com.moises.mydocs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import br.com.moises.mydocs.R
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.model.Entries
import br.com.moises.mydocs.model.FileType
import br.com.moises.mydocs.util.DateUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_document_file.view.*


/**
 * Created by Filipe on 12/04/2018.
 */
class DocumentsAdapter(val context: Context, var documents: List<Entries> = emptyList()) : BaseAdapter() {

    override fun getItem(position: Int): Any {
        return documents[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (view == null) {
            view = inflater.inflate(R.layout.item_document_file, parent, false)
        }
        val item = getItem(position) as Entries
        view!!.tv_name.text = item.name
        view.tv_date.text = DateUtil.formatStringDate(item.lastmodified)
        view.iv_status.setImageResource(if (item.localStore) R.drawable.ic_cloud_done_light_green_500_24dp else R.drawable.ic_cloud_download_black_24dp)
        view.tv_extension.text = item.contentsettings.contenttype.split("/").get(1)
        when (FileType.getByMimeType(item.contentsettings.contenttype)) {
            FileType.AUDIO -> {
                view.iv_type.setImageResource(R.drawable.ic_audiotrack_blue_grey_500_48dp)
            }
            FileType.VIDEO -> {
                view.iv_type.setImageResource(R.drawable.ic_videocam_blue_grey_500_48dp)
            }
            FileType.DOC -> {
                view.iv_type.setImageResource(R.drawable.ic_description_blue_grey_500_48dp)
            }
            FileType.IMAGE -> {
                Glide.with(ContextHelper.applicationContext)
                        .load(item.getFileUrl())
                        .into(view.iv_type);
            }
        }

        view.setOnClickListener { onClickItem(item) }
        return view
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return documents.size
    }

    var onClickItem: (item: Entries) -> Unit = { _ -> }

}