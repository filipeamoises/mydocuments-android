package br.com.moises.mydocs.activity

import android.os.Bundle
import br.com.moises.mydocs.R
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.business.login.LoginBusiness
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.Login
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.util.TextUtil
import kotlinx.android.synthetic.main.activity_login.*


open class LoginActivity : SimpleActivity(R.layout.activity_login, R.string.title_login) {

    private var receiver = Receiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bindViews()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onResume() {
        super.onResume()
        LoginBusiness.registerReceiver(receiver, this::class)
    }

    override fun onPause() {
        super.onPause()
        LoginBusiness.removeReceiver(receiver)
    }

    private fun bindViews() {
        bt_login.setOnClickListener { callService() }
        bt_register.setOnClickListener { goToRegisterActivity() }
    }

    private fun goToRegisterActivity() {
        ActivityHelper.startActivityFinishingPrevious(SignUpActivity::class.java)
    }

    private fun callService() {
        if (isValidFields()) {
            showLoading()

            val login = Login()

            login.email = field_email.text
            login.password = field_password.text

            val intent = LoginBusiness.makeIntent(this::class, login)
            startService(intent)
        }
    }

    private fun isValidFields(): Boolean {
        if (!TextUtil.validateEmail(field_email.text)) {
            field_email.error = getString(R.string.err_email_format)
            return false
        }
        if (field_password.text.isEmpty()) {
            field_password.error = getString(R.string.err_password_presence)
            return false
        }

        return true
    }

    fun onLoginSuccess(auth: Auth) {
        PreferencesHelper.set(PreferenceKeys.APP_FIRST_ACCESS, false)
        PreferencesHelper.set(PreferenceKeys.USER_FIRST_LOGIN, false)
        PreferencesHelper.set(PreferenceKeys.APP_SESSION_TOKEN, auth.token)
        PreferencesHelper.set(PreferenceKeys.USER_ID, auth.userId)

        ActivityHelper.startActivityFinishingPrevious(BottomNavigationActivity::class.java)

        hideLoading()
    }

    private inner class Receiver : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            onLoginSuccess(bundle.getParcelable<Auth>(BaseBusiness.REST_SUCCESS_PARCELABLE_ITEM))
        }
    }
}
