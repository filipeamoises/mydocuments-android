package br.com.moises.mydocs.helper

import android.Manifest
import android.support.v7.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

/**
 * Created by Filipe on 11/04/2018.
 */
object PermissionsHelper {

    fun checkPermissions(activity: AppCompatActivity,
                         permissions: Permissions = Permissions.ALL,
                         onSuccess: () -> Unit,
                         onError: (deniedPermissions: List<String>) -> Unit = { }) {
        Dexter.withActivity(activity)
                .withPermissions(permissions.list.asList())
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report != null && report.areAllPermissionsGranted()) {
                            onSuccess()
                        } else {
                            onError(report!!.deniedPermissionResponses.map { it.permissionName })
                        }
                    }

                }).check()
    }

    enum class Permissions(vararg var list: String) {

        CAMERA(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE),

        AUDIO(
                Manifest.permission.RECORD_AUDIO),

        FILE(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE),

        LOCATION(
                Manifest.permission.ACCESS_FINE_LOCATION),

        ALL(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION),
    }
}