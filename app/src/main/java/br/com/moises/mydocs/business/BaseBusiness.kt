package br.com.moises.mydocs.business

import android.app.IntentService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.LocalBroadcastManager
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.rest.JsonRestReponseCallback
import java.util.*
import kotlin.reflect.KClass

/**
 * Base of ALL business
 * Created by Filipe on 10/04/2018.
 */

open class BaseBusiness<R>(val successAction: String, val tag: String = "") : IntentService(tag), JsonRestReponseCallback<R> {

    companion object {

        val REST_SUCCESS_PARCELABLE_ITEM: String = "REST_SUCCESS_PARCELABLE_ITEM"
        val REST_ERROR_ITEM: String = "REST_ERROR_ITEM"
        val FAILURE_ITEM: String = "FAILURE_ITEM"
        val PROBLEM_API_RESPONSE_INTENT_ACTION: String = "PROBLEM_API_RESPONSE_INTENT_ACTION"
        val FAILURE_INTENT_ACTION: String = "FAILURE_INTENT_ACTION"

        private val CALLER_CLASS_NAME = "CALLER_CLASS_NAME"

        val CACHE_KEY = "CACHE_KEY"

        fun getFailureMessage(intent: Intent): String = intent.getStringExtra(FAILURE_ITEM)

        fun getProblemApiResponse(intent: Intent): ProblemApiResponse = intent.getParcelableExtra(REST_ERROR_ITEM)

        fun buildAction(action: String, callerClassName: String): String = "$action*${callerClassName}"

        fun getAction(intent: Intent): String = intent.action.split("*")[0]

        fun buildSuccessIntentFilter(action: String, callerClassName: String): IntentFilter = IntentFilter(buildAction(action, callerClassName))

        fun buildErrorIntentFilter(callerClassName: String): IntentFilter {
            val intentFilter = IntentFilter()
            val actions = arrayOf(
                    BaseBusiness.REST_SUCCESS_PARCELABLE_ITEM,
                    BaseBusiness.REST_ERROR_ITEM,
                    BaseBusiness.FAILURE_ITEM,
                    BaseBusiness.FAILURE_INTENT_ACTION,
                    BaseBusiness.PROBLEM_API_RESPONSE_INTENT_ACTION)

            actions.forEach { a ->
                intentFilter.addAction(buildAction(a, callerClassName))
            }

            return intentFilter
        }


        internal fun makeIntent(classCaller: KClass<*>, serviceClass: KClass<*>): Intent {
            val intent = Intent(ContextHelper.applicationContext, serviceClass.java)

            intent.putExtra(CALLER_CLASS_NAME, classCaller.java.canonicalName)
            intent.putExtra(CACHE_KEY, getCacheKey(classCaller, serviceClass))

            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>, action: String) {
            val intentFilter = buildSuccessIntentFilter(action, callerClass.java.canonicalName!!)

            LocalBroadcastManager.getInstance(ContextHelper.applicationContext).registerReceiver(receiver, intentFilter)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            LocalBroadcastManager.getInstance(ContextHelper.applicationContext).unregisterReceiver(receiver)
        }

        fun getCacheKey(classCaller: KClass<*>, serviceClass: KClass<*>): String = "${serviceClass.java.canonicalName}:${classCaller.java.canonicalName}"

        fun <R> getRestSuccess(bundle: Bundle): R = bundle.get(REST_SUCCESS_PARCELABLE_ITEM) as R
    }

    override fun onHandleIntent(intent: Intent?) {
        configureCallerClassName(intent)

        cacheKey = intent?.getStringExtra(BaseBusiness.CACHE_KEY) ?: ""
    }

    protected var cacheKey: String = ""

    private var _callerClassName: String = ""

    internal fun getCallerClassName(): String {
        return _callerClassName
    }

    internal fun configureCallerClassName(intent: Intent?) {
        this._callerClassName = intent!!.getStringExtra(CALLER_CLASS_NAME)
    }


    fun buildAction(action: String): String = BaseBusiness.buildAction(action, getCallerClassName())

    fun getAction(intent: Intent): String = BaseBusiness.getAction(intent)

    fun buildSuccessIntentFilter(action: String): IntentFilter = BaseBusiness.buildSuccessIntentFilter(action, getCallerClassName())

    fun buildErrorIntentFilter(): IntentFilter = BaseBusiness.buildErrorIntentFilter(getCallerClassName())

    override fun onRestSuccess(response: R?) {
        val intent = Intent(buildAction(successAction, getCallerClassName()))

        when (response) {
            is Parcelable -> intent.putExtra(REST_SUCCESS_PARCELABLE_ITEM, response)
            is List<*> -> intent.putParcelableArrayListExtra(REST_SUCCESS_PARCELABLE_ITEM, response as ArrayList<out Parcelable>)
        }

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).sendBroadcast(intent)
    }

    override fun onRestError(response: ProblemApiResponse) {
        val intent = Intent(buildAction(PROBLEM_API_RESPONSE_INTENT_ACTION, getCallerClassName()))

        intent.putExtra(REST_ERROR_ITEM, response)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).sendBroadcast(intent)
    }

    override fun onFailure(t: Throwable) {
        val intent = Intent(buildAction(FAILURE_INTENT_ACTION, getCallerClassName()))

        intent.putExtra(FAILURE_ITEM, t.message)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).sendBroadcast(intent)
    }

    open fun getClientFailureMessage(intent: Intent): String = getFailureMessage(intent)

    open fun getRestProblemApiResponse(intent: Intent): ProblemApiResponse = getProblemApiResponse(intent)

    open fun getRestSuccess(bundle: Bundle): R = bundle.get(REST_SUCCESS_PARCELABLE_ITEM) as R

    abstract class Receiver : BroadcastReceiver() {

        override final fun onReceive(context: Context?, intent: Intent?) {
            onReceive(getAction(intent!!), intent.extras ?: Bundle.EMPTY)
        }

        abstract fun onReceive(action: String, bundle: Bundle)
    }
}

