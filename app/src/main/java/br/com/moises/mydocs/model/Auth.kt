package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 12/04/2018.
 */
open class Auth(
        var auth: kotlin.Boolean? = null,
        var token: kotlin.String? = null,
        var userId: kotlin.Int? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        this.auth?.let { parcel.writeValue(it) }
        parcel.writeString(token)
        parcel.writeInt(userId!!)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Auth> {

        override fun createFromParcel(parcel: Parcel): Auth = Auth(parcel)

        override fun newArray(size: Int): Array<Auth?> = arrayOfNulls(size)
    }
}