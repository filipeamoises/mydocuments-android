package br.com.moises.mydocs.business.document

import android.content.BroadcastReceiver
import android.content.Intent
import android.net.Uri
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.rest.document.DocumentUploadRestService
import br.com.moises.mydocs.util.toFile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import kotlin.reflect.KClass

/**
 * Created by Filipe on 12/04/2018.
 */
class DocumentUploadBusiness : BaseBusiness<Void>(ACTION_SUCCESS, TAG) {

    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)

        val uriPath = intent!!.getStringExtra(KEY_URI_PATH)

        callService(uriPath)
    }

    private fun callService(uriPath: String) {
        val file = Uri.parse(uriPath).toFile()
        val service = DocumentUploadRestService(prepareFilePart(file), this)

        service.start()
    }

    private fun prepareFilePart(file: File): MultipartBody.Part {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

        return MultipartBody.Part.createFormData("file", file.name, requestFile)
    }

    companion object {

        private val CLASS = DocumentUploadBusiness::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_document_upload_success"

        private val KEY_URI_PATH = "key_file_path"

        fun makeIntent(classCaller: KClass<*>, uriPath: String): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)

            intent.putExtra(KEY_URI_PATH, uriPath)

            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>) {
            BaseBusiness.registerReceiver(receiver, callerClass, ACTION_SUCCESS)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            BaseBusiness.removeReceiver(receiver)
        }
    }
}