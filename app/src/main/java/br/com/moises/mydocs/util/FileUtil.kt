package br.com.moises.mydocs.util

import android.net.Uri
import java.io.File

/**
 * Created by Filipe on 11/04/2018.
 */
fun Uri.toFile(): File = File(this.path)