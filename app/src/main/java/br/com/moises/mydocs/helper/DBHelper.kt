package br.com.moises.mydocs.helper

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import br.com.moises.mydocs.dao.image.ImageContract

/**
 * Created by Filipe on 10/04/2018.
 */
internal class DBHelper : SQLiteOpenHelper(ContextHelper.applicationContext, DATABASE_NAME, null, DATABASE_VERSION) {

    private val fileContract = ImageContract()

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(fileContract.createTable())
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {

        val DATABASE_NAME = "mydocs.db"

        private val DATABASE_VERSION = 1
    }
}