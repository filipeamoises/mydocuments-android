package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 11/04/2018.
 */
open class DocumentSpecs(

        var id: Long? = null,
        var name: String? = null,
        var type: FileType = FileType.NONE,
        var uploadDate: String? = null,
        var urlFile: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            FileType.getById(parcel.readInt()),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeInt(type.id)
        parcel.writeString(uploadDate)
        parcel.writeString(urlFile)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<DocumentSpecs> {

        override fun createFromParcel(parcel: Parcel): DocumentSpecs = DocumentSpecs(parcel)

        override fun newArray(size: Int): Array<DocumentSpecs?> = arrayOfNulls(size)
    }
}