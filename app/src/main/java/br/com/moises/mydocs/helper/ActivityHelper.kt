package br.com.moises.mydocs.helper

import android.content.Intent

/**
 * Created by Filipe on 10/04/2018.
 */
object ActivityHelper {

    fun startActivity(activityClass: Class<*>) {
        val intent = Intent(ContextHelper.applicationContext, activityClass)

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        ContextHelper.applicationContext.startActivity(intent)
    }

    fun startActivityFinishingPrevious(activityClass: Class<*>) {
        val intent = Intent(ContextHelper.applicationContext, activityClass)

        startActivityFinishingPrevious(intent)
    }

    fun startActivityFinishingPrevious(intent: Intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

        ContextHelper.applicationContext.startActivity(intent)
    }
}