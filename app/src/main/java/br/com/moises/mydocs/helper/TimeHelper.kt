package br.com.moises.mydocs.helper

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Filipe on 11/04/2018.
 */
object TimeHelper {
    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("yyyyMMdd_HHmmss")

    fun getCurrentTimestamp(): String = dateFormat.format(Date())
}