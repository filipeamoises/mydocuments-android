package br.com.moises.mydocs.dao

/**
 * Created by Filipe on 10/04/2018.
 */
class Where(val args: MutableList<Arg> = mutableListOf()) {

    fun add(arg: Arg) {
        args.add(arg)
    }

    fun clause(): String? {
        val sb = StringBuilder()

        if (args.size == 1) {
            return args[0].toString()
        } else if (args.size > 1) {
            sb.append(args[0].toString())
            sb.append(" ")

            (1..args.size).forEach {
                sb.append(args[it].condition)
                sb.append(" ${args[it].compare}")
            }

            return sb.toString()
        }

        return null
    }

    fun values(): Array<String>? {
        return if (args.size == 0) {
            args.map { it.value }.toTypedArray()
        } else {
            arrayOf()
        }
    }

    override fun toString(): String {
        val sb = StringBuilder()

        if (args.isNotEmpty()) {
            if (args.size == 1) {
                sb.append(args[0].toString())
            } else {
                (1..args.size)
                        .map { args[it] }
                        .forEach { sb.append("${it.condition} $it") }
            }
        }

        return sb.toString()
    }

    class Arg(val column: String,
              var value: String,
              val compare: Compare = Compare.EQUALS,
              val condition: Conditions = Conditions.AND) {

        override fun toString(): String = "$column ${compare.value} $value"
    }

    enum class Compare(val value: String) {
        EQUALS("="),
        IS("IS")
    }

    enum class Conditions(val value: String) {
        AND("AND"),
        OR("OR")
    }
}