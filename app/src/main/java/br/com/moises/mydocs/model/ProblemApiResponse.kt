package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 10/04/2018.
 */
open class ProblemApiResponse (
        var title: kotlin.String? = null,
        var detail: kotlin.String? = null,
        var status: kotlin.String? = null,
        var type: kotlin.String? = null,
        var instance: kotlin.String? = null,
        var code: kotlin.String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(detail)
        parcel.writeString(status)
        parcel.writeString(type)
        parcel.writeString(instance)
        parcel.writeString(code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProblemApiResponse> {
        override fun createFromParcel(parcel: Parcel): ProblemApiResponse {
            return ProblemApiResponse(parcel)
        }

        override fun newArray(size: Int): Array<ProblemApiResponse?> {
            return arrayOfNulls(size)
        }
    }

}