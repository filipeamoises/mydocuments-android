package br.com.moises.mydocs.helper

import android.app.Dialog
import android.content.Context
import android.support.v7.app.AlertDialog

/**
 * Created by Filipe on 10/04/2018.
 */
class DialogHelper(val context: Context) {

    var type: Types = Types.INFO

    var title: String = ""

    var message: String = ""

    var listener: (Button) -> Unit = {}

    var dialog: Dialog? = null

    fun show() {
        val builder = AlertDialog.Builder(context)

        builder.setTitle(title)
        builder.setMessage(message)

        when (type) {
            Types.YES_NO -> {
                builder.setPositiveButton(android.R.string.yes) { dialog, which -> listener(Button.YES) }
                builder.setNegativeButton(android.R.string.no) { dialog, which -> listener(Button.NO) }
                builder.setOnCancelListener { listener(Button.OK) }
            }
            else -> {
                builder.setNeutralButton(android.R.string.ok) { dialog, which -> listener(Button.OK) }
                builder.setOnCancelListener { listener(Button.OK) }
            }
        }

        dialog = builder.show()
    }

    fun hide() {
        dialog?.dismiss()
    }

    class Builder(context: Context) {

        val obj: DialogHelper = DialogHelper(context)

        fun type(type: Types): Builder {
            obj.type = type
            return this
        }

        fun title(title: String): Builder {
            obj.title = title
            return this
        }

        fun message(message: String): Builder {
            obj.message = message
            return this
        }

        fun listener(listener: (button: Button) -> Unit): Builder {
            obj.listener = listener
            return this
        }

        fun show() {
            obj.show()
        }
    }

    enum class Types {

        INFO, ERROR, YES_NO
    }

    enum class Button {

        OK, YES, NO
    }
}