package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 12/04/2018.
 */
open class Login(

        var email: kotlin.String? = null,
        var password: kotlin.String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(password)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Login> {

        override fun createFromParcel(parcel: Parcel): Login = Login(parcel)

        override fun newArray(size: Int): Array<Login?> = arrayOfNulls(size)
    }
}