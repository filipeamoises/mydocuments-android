package br.com.moises.mydocs.activity

import android.support.v7.widget.Toolbar
import android.view.Gravity
import br.com.moises.mydocs.R
import br.com.moises.mydocs.util.ResUtil
import br.com.moises.mydocs.view.ActivityView

/**
 * Created by Filipe on 11/04/2018.
 */
abstract class SimpleActivity(var layout: Int, private val title: Int = R.string.app_name) : BaseActivity<ActivityView>(layout, title) {


    fun setInitialActivity() {

        val params = rootView.toolbarTitle.layoutParams as Toolbar.LayoutParams
        params.gravity = Gravity.CENTER

        rootView.toolbarTitle.layoutParams = params

        supportActionBar!!.elevation = 0f

        ResUtil.tint(rootView.appbar, android.R.color.transparent)
        ResUtil.tint(rootView.toolbar, android.R.color.transparent)
        ResUtil.tint(rootView.toolbarTitle, android.R.color.white)
    }
}