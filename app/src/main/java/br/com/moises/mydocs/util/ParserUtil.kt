package br.com.moises.mydocs.util

import com.google.gson.JsonSyntaxException
import java.io.IOException
import java.io.InputStream

/**
 * Created by Filipe on 11/04/2018.
 */
interface ParserUtil<T> {
    /**
     * Parses any kind of Json Object. JSON Deserialization fom jsonContent to T
     * type.
     *
     * @param jsonContent
     * @return T typed object.
     * @throws JsonSyntaxException
     * @throws IOException
     */
    @Throws(JsonSyntaxException::class, IOException::class)
    fun parse(jsonContent: String): T

    /**
     * Parses any kind of Json Object. JSON Deserialization fom jsonContent to T
     * type.
     *
     * @param jsonContent
     * @return T typed object.
     * @throws IOException
     */
    @Throws(JsonSyntaxException::class, IOException::class)
    fun parse(jsonContent: InputStream): T

    /**
     *
     * @param t
     * @return
     * @throws JsonSyntaxException
     * @throws IOException
     */
    @Throws(JsonSyntaxException::class, IOException::class)
    fun toJson(t: T): ByteArray
}
