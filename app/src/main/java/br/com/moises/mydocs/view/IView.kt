package br.com.moises.mydocs.view

import android.support.constraint.ConstraintLayout
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.Toolbar
import android.widget.TextView

/**
 * Created by Filipe on 10/04/2018.
 */
interface IView {

    var appbar: AppBarLayout

    var toolbar: Toolbar

    var toolbarTitle: TextView

    var contentView: ConstraintLayout

    var loadingView: ConstraintLayout

    fun showLoading()

    fun hideLoading()
}