package br.com.moises.mydocs.rest

import okhttp3.ResponseBody

/**
 * Created by Filipe on 11/04/2018.
 */
interface InputStreamRestReponseCallback : RestReponseCallback<ResponseBody> {

    fun onRestSuccess(response: ByteArray)

}