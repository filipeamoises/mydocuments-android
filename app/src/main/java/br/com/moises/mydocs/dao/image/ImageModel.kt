package br.com.moises.mydocs.dao.image

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 10/04/2018.
 */
open class ImageModel(
        var id: Long? = null,
        var userId: Long? = null,
        var uri: String? = null,
        var hash: String? = null,
        var uploaded: Boolean = false
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as Boolean)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(userId)
        parcel.writeString(uri)
        parcel.writeString(hash)
        parcel.writeValue(uploaded)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ImageModel> {

        override fun createFromParcel(parcel: Parcel): ImageModel = ImageModel(parcel)

        override fun newArray(size: Int): Array<ImageModel?> = arrayOfNulls(size)
    }
}
