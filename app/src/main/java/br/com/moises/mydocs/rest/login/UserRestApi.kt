package br.com.moises.mydocs.rest.login

import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.Login
import br.com.moises.mydocs.model.SignUp
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by Filipe on 12/04/2018.
 */
interface UserRestApi {

//    @Headers("Content-type: application/json")
    @POST("auth/login")
    fun login(@Body login: Login): Call<Auth>

//    @Headers("Content-type: application/json")
    @POST("auth/register")
    fun signUp(@Body signup: SignUp): Call<Auth>

    @Headers("Content-type: application/json")
    @POST("usuario/logout")
    fun logout(): Call<Void>
}