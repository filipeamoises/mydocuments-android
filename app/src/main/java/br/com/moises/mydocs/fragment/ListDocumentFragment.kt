package br.com.moises.mydocs.fragment

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import br.com.moises.mydocs.R
import br.com.moises.mydocs.activity.LoginActivity
import br.com.moises.mydocs.adapter.DocumentsAdapter
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.business.document.DocumentDownloadBusiness
import br.com.moises.mydocs.business.document.DocumentListBusiness
import br.com.moises.mydocs.helper.*
import br.com.moises.mydocs.model.Download
import br.com.moises.mydocs.model.Entries
import br.com.moises.mydocs.model.FileType
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.util.DateUtil
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.fragment_list_document.*
import java.io.File
import java.util.*


/**
 * Created by Filipe on 11/04/2018.
 */
class ListDocumentFragment : BaseFragment(R.layout.fragment_list_document) {

    private var receiverListDocuments = ReceiverListDocuments()
    private var receiverDownloadDocuments = ReceiverDownloadDocument()
    private lateinit var adapter: DocumentsAdapter
    private var listEntries: List<Entries> = ArrayList<Entries>()
    private var sorted = false

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true);
        bindView()
        callServiceListDocument()
        Fabric.with(ContextHelper.applicationContext, Answers())
    }

    private fun bindView() {
        fab_upload.setOnClickListener { showSelectFileDialog() }
        bindList()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_toolbar_documents, menu)
        menu!!.findItem(R.id.action_sort)?.setOnMenuItemClickListener() { sortFiles() }
        menu!!.findItem(R.id.action_logout)?.setOnMenuItemClickListener() { logout() }

        val myActionMenuItem = menu.findItem(R.id.action_search)
        var searchView = myActionMenuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                var listSearch = ArrayList<Entries>()
                for (item in listEntries) {
                    if (item.name.contains(query) || DateUtil.formatStringDate(item.lastmodified).contains(query))
                        listSearch.add(item)
                }
                adapter.documents = listSearch
                adapter.notifyDataSetChanged()
                if (!searchView.isIconified()) {
                    searchView.setIconified(true)
                }
                myActionMenuItem.collapseActionView()
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                var listSearch = ArrayList<Entries>()
                for (item in listEntries) {
                    if (item.name.contains(s) || DateUtil.formatStringDate(item.lastmodified).contains(s))
                        listSearch.add(item)
                }
                adapter.documents = listSearch
                adapter.notifyDataSetChanged()
                return false
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun logout(): Boolean {
        PreferencesHelper.reset()
        ActivityHelper.startActivityFinishingPrevious(LoginActivity::class.java)
        return true
    }

    private fun sortFiles(): Boolean {
        var list = adapter.documents
        adapter.documents = list.sortedWith(if (sorted) compareByDescending({ it.lastmodified }) else compareBy({ it.lastmodified }))
        adapter.notifyDataSetChanged()
        sorted = !sorted
        return true
    }

    //Realize the bind and configure list
    private fun bindList() {
        checkLocalFiles()
        adapter = DocumentsAdapter(ContextHelper.applicationContext, listEntries)
        rv_list_documents.adapter = adapter
        sr_swiperefresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener { callServiceListDocument() })
        adapter.onClickItem = { item -> openDialogActions(item) }
        tv_no_documents.visibility = if (listEntries.isEmpty()) View.VISIBLE else View.GONE
        rv_list_documents.visibility = if (listEntries.isEmpty()) View.GONE else View.VISIBLE
    }

    //    Check the files on local folder
    private fun checkLocalFiles() {
        val directory = File(Environment.getExternalStorageDirectory(), getString(R.string.directory_name))
        val files = directory.listFiles()
        if (files != null) {
            for (local in files) {
                for (cloud in listEntries) {
                    if (cloud.name.equals(local.name)) {
                        cloud.localStore = true
                    }
                }
            }
        }
    }

    private fun openDialogActions(item: Entries) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)
        builder.setTitle(FileType.getByMimeType(item.contentsettings.contenttype).name)
        builder.setMessage(R.string.lbl_what_to_do)

        builder.setNegativeButton(R.string.lbl_nothing, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, index: Int) {
                dialog.dismiss()
            }
        })
        if (item.localStore) {
            builder.setPositiveButton(R.string.lbl_share, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, index: Int) {
                    shareFile(item)
                    dialog.dismiss()
                }
            })
            builder.setNeutralButton(R.string.lbl_delete_local, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, index: Int) {
                    deleteLocalFile(item)
                    dialog.dismiss()
                }
            })
        } else {
            builder.setPositiveButton(R.string.lbl_download, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, index: Int) {
                    downloadFile(item)
                    dialog.dismiss()
                }
            })
        }

        builder.setCancelable(true)
        builder.show()
    }

    private fun deleteLocalFile(item: Entries) {
        val fileWithinMyDir = File(File(Environment.getExternalStorageDirectory(), getString(R.string.directory_name)).path, item.name)
        if (fileWithinMyDir.exists()) {
            if (fileWithinMyDir.delete()) {
                Toast.makeText(ContextHelper.applicationContext, getString(R.string.msg_file_delete_local), Toast.LENGTH_SHORT).show()
                callServiceListDocument()
            } else {
                Toast.makeText(ContextHelper.applicationContext, getString(R.string.err_delete_local), Toast.LENGTH_SHORT).show()
            }
        }

    }

    //Method to download file
    private fun downloadFile(item: Entries) {
        val intent = DocumentDownloadBusiness.makeIntent(this::class)
        intent.putExtra(DocumentDownloadBusiness.INTENT_REQUEST, item)
        context.startService(intent)
    }


    //Method to share files to other apps
    private fun shareFile(item: Entries) {
        val intentShareFile = Intent(Intent.ACTION_SEND)
        val fileWithinMyDir = File(File(Environment.getExternalStorageDirectory(), getString(R.string.directory_name)).path, item.name)

        if (fileWithinMyDir.exists()) {
            intentShareFile.type = item.contentsettings.contenttype
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(fileWithinMyDir))

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Sharing File...")
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...")

            startActivity(Intent.createChooser(intentShareFile, "Share File"))
        }
    }

    private fun showSelectFileDialog() {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)
        val dialogView = this.getLayoutInflater().inflate(R.layout.layout_dialog_file, null)
        builder.setTitle(R.string.lbl_empty)
        builder.setView(dialogView)
        builder.show()
        bindUploadButtons(dialogView)
    }

    //    bind dialog buttons
    private fun bindUploadButtons(view: View) {
        val ivCamera = view.findViewById<ImageView>(R.id.iv_camera)
        val ivAudio = view.findViewById<ImageView>(R.id.iv_audio)
        val ivDocs = view.findViewById<ImageView>(R.id.iv_doc)
        val ivVideo = view.findViewById<ImageView>(R.id.iv_video)

        // Camera button
        ivCamera.setOnClickListener({
            PermissionsHelper.checkPermissions(
                    activity = activity as AppCompatActivity,
                    permissions = PermissionsHelper.Permissions.CAMERA,
                    onSuccess = { FileHelper.getFile(activity as AppCompatActivity, FileHelper.Type.IMAGE) },
                    onError = { permissionError(it) })
        })

        // Video button
        ivVideo.setOnClickListener({
            PermissionsHelper.checkPermissions(
                    activity = activity as AppCompatActivity,
                    permissions = PermissionsHelper.Permissions.CAMERA,
                    onSuccess = { FileHelper.getFile(activity as AppCompatActivity, FileHelper.Type.VIDEO) },
                    onError = { permissionError(it) })
        })

        // Docs button
        ivDocs.setOnClickListener({
            PermissionsHelper.checkPermissions(
                    activity = activity as AppCompatActivity,
                    permissions = PermissionsHelper.Permissions.FILE,
                    onSuccess = { FileHelper.getFile(activity as AppCompatActivity, FileHelper.Type.DOC) },
                    onError = { permissionError(it) })
        })

        // Audio button
        ivAudio.setOnClickListener({
            PermissionsHelper.checkPermissions(
                    activity = activity as AppCompatActivity,
                    permissions = PermissionsHelper.Permissions.AUDIO,
                    onSuccess = {
                        FileHelper.getFile(activity as AppCompatActivity, FileHelper.Type.AUDIO, onFinished = { file ->
                            val arquivo = FileHelper.getModelFile(file)
//                            attachFile(arquivo)
                        })
                    },
                    onError = { permissionError(it) })
        })
    }

    private fun permissionError(map: List<String>) {
        Toast.makeText(ContextHelper.applicationContext, map[0], Toast.LENGTH_SHORT).show()
    }


    private fun callServiceListDocument() {
        sr_swiperefresh.isRefreshing = true
        val intent = DocumentListBusiness.makeIntent(this::class)
        context.startService(intent)
    }

    override fun onRestFailure(response: String) {
        sr_swiperefresh.isRefreshing = false
        super.onRestFailure(response)
    }

    override fun onRestError(response: ProblemApiResponse) {
        sr_swiperefresh.isRefreshing = false
        super.onRestError(response)
    }

    override fun onResume() {
        super.onResume()
        DocumentListBusiness.registerReceiver(receiverListDocuments, this::class)
        DocumentDownloadBusiness.registerReceiver(receiverDownloadDocuments, this::class)
    }

    override fun onPause() {
        super.onPause()
        DocumentListBusiness.removeReceiver(receiverListDocuments)
        DocumentDownloadBusiness.removeReceiver(receiverDownloadDocuments)
    }

    private inner class ReceiverListDocuments : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            sr_swiperefresh.isRefreshing = false
            listEntries = BaseBusiness.getRestSuccess<List<Entries>>(bundle)
            bindList()

        }
    }

    private inner class ReceiverDownloadDocument : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            BaseBusiness.getRestSuccess(bundle) as Download
            callServiceListDocument()
        }
    }
}