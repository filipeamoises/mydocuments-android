package br.com.moises.mydocs.activity

import android.os.Bundle
import br.com.moises.mydocs.R
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.business.login.SignUpBusiness
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.model.SignUp
import br.com.moises.mydocs.util.TextUtil
import kotlinx.android.synthetic.main.activity_register.*

open class SignUpActivity : SimpleActivity(R.layout.activity_register, R.string.title_login) {
    private var receiver = Receiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setInitialActivity()
        bindViews()
    }

    override fun onResume() {
        super.onResume()
        SignUpBusiness.registerReceiver(receiver, this::class)
    }

    override fun onPause() {
        super.onPause()
        SignUpBusiness.removeReceiver(receiver)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityHelper.startActivityFinishingPrevious(LoginActivity::class.java)
    }

    private fun bindViews() {
        bt_send.setOnClickListener({ callService() })
        bt_login.setOnClickListener { ActivityHelper.startActivityFinishingPrevious(LoginActivity::class.java) }
    }

    private fun callService() {

        if (isValidFields()) {
            showLoading()

            val signUp = SignUp()

            signUp.name = field_name.text
            signUp.email = field_email.text
            signUp.password = field_password.text
            signUp.phone = field_phone.text

            val intent = SignUpBusiness.makeIntent(this::class, signUp)
            startService(intent)
        }
    }

    private fun isValidFields(): Boolean {
        if (field_name.text.length < 3) {
            field_name.error = getString(R.string.err_name_presence)
            return false
        }
        if (field_email.text.length < 3) {
            field_email.error = getString(R.string.err_email_presence)
            return false
        } else if (!TextUtil.validateEmail(field_email.text)) {
            field_email.error = getString(R.string.err_email_format)
            return false
        }
        if (field_password.text.isEmpty()) {
            field_password.error = getString(R.string.err_password_presence)
            return false
        }
        if(field_phone.text.length != 11){
            field_phone.error = getString(R.string.err_phone_format)
            return false
        }

        return true
    }

    private fun onSignUpSuccess(auth: Auth) {
        PreferencesHelper.set(PreferenceKeys.APP_FIRST_ACCESS, false)
        PreferencesHelper.set(PreferenceKeys.USER_FIRST_LOGIN, true)
        PreferencesHelper.set(PreferenceKeys.USER_EMAIL, field_email.text)
        PreferencesHelper.set(PreferenceKeys.APP_SESSION_TOKEN, auth.token)
        PreferencesHelper.set(PreferenceKeys.USER_ID, auth.userId)
        ActivityHelper.startActivityFinishingPrevious(BottomNavigationActivity::class.java)
        hideLoading()
    }

    private inner class Receiver : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            onSignUpSuccess(BaseBusiness.getRestSuccess<Auth>(bundle))
        }
    }
}