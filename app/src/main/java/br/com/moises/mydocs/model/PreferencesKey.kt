package br.com.moises.mydocs.model

import br.com.moises.mydocs.helper.PreferencesHelper

/**
 * Created by Filipe on 10/04/2018.
 */
enum class PreferenceKeys(val key: String, val defaultValue: Any) {

    APP_PREFERENCE_VERSION("app_preference_version", PreferencesHelper.VERSION),
    APP_FIRST_ACCESS("app_first_access", true),
    APP_SESSION_TOKEN("app_session_token", ""),
    USER_FIRST_LOGIN("ser_first_login", true),
    USER_EMAIL("user_email", ""),
    USER_ID("user_id", -1),
    USER_NAME("user_name", "");
}