package br.com.moises.mydocs.dao

import android.content.ContentValues
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.os.Parcelable
import android.util.AndroidRuntimeException
import br.com.moises.mydocs.helper.DBHelper
import java.util.*
import java.util.concurrent.locks.ReentrantLock

/**
 * Created by Filipe on 10/04/2018.
 */
internal abstract class BaseDAO<M : Parcelable> {

    abstract protected val contract: BaseContract

    private val dbHelper = DBHelper()

    protected lateinit var database: SQLiteDatabase

    private val cursors = ArrayList<Cursor>()

    private val lock = ReentrantLock()

    fun open(writable: Boolean = false) {
        database = if (writable) dbHelper.writableDatabase else dbHelper.readableDatabase
    }

    fun close() {
        if (lock.tryLock()) {
            try {
                if (!cursors.isEmpty()) {
                    cursors.filterNot { it.isClosed }.forEach { it.close() }
                    cursors.clear()
                }

                if (isOpened())
                    database.close()
            } finally {
                lock.unlock()
            }
        }
    }

    fun manageCursor(cursor: Cursor) {
        if (lock.tryLock()) {
            try {
                this.cursors.add(cursor)
            } finally {
                lock.unlock()
            }
        }
    }

    @Synchronized
    fun haCursorPronto(): Boolean {
        if (cursors.isEmpty()) {
            return false
        }
        val lastCursor = cursors[cursors.size - 1]
        return isOpened() && lastCursor.isBeforeFirst && lastCursor.count > 0
    }

    @Synchronized
    private fun cursor(): Cursor? {
        return if (cursors.isEmpty()) {
            null
        } else cursors[cursors.size - 1]
    }

    @Synchronized
    fun startCursor(): Cursor? {
        var lastCursor: Cursor? = null
        if (haCursorPronto()) {
            lastCursor = cursors[cursors.size - 1]
            lastCursor.moveToNext()
        }
        return lastCursor
    }

    @Synchronized
    private fun isOpened(): Boolean = database.isOpen

    fun beginTransaction() {
        if (lock.tryLock()) {
            try {
                database.beginTransaction()
            } finally {
                lock.unlock()
            }
        }
    }

    fun endTransaction() {
        if (lock.tryLock()) {
            try {
                database.endTransaction()
            } finally {
                lock.unlock()
            }
        }
    }

    @Synchronized
    fun inTransaction(): Boolean = database.inTransaction()

    fun setTransactionSuccessful() {
        if (lock.tryLock()) {
            try {
                database.setTransactionSuccessful()
            } finally {
                lock.unlock()
            }
        }
    }

    protected fun getLong(cursor: Cursor, columnName: String): Long? {
        return try {
            cursor.getLong(cursor.getColumnIndex(columnName))
        } catch (e: Exception) {
            null
        }
    }

    protected fun getInt(cursor: Cursor, columnName: String): Int? {
        return try {
            cursor.getInt(cursor.getColumnIndex(columnName))
        } catch (e: Exception) {
            null
        }
    }

    protected fun getString(cursor: Cursor, conlumnName: String): String? {
        return try {
            cursor.getString(cursor.getColumnIndex(conlumnName))
        } catch (e: Exception) {
            null
        }
    }

    protected fun getBoolean(cursor: Cursor, columnName: String): Boolean? {
        return try {
            cursor.getInt(cursor.getColumnIndex(columnName)) == 1
        } catch (e: Exception) {
            null
        }
    }

    protected fun getDate(cursor: Cursor, columnName: String): Date? {
        return try {
            val dateInLong = cursor.getLong(cursor.getColumnIndex(columnName))

            if (dateInLong > 0) Date(dateInLong) else null
        } catch (e: Exception) {
            null
        }
    }

    @Synchronized
    fun query(columns: Array<String>, selection: String, selectionArgs: Array<String>, groupBy: String, having: String, orderBy: String): Cursor {
        val managedCursor = database.query(contract.tableName, columns, selection, selectionArgs, groupBy, having, orderBy)

        manageCursor(managedCursor)

        return managedCursor
    }

    @Synchronized
    open fun select(where: Where): List<M> {
        val list = ArrayList<M>()

        try {
            open(false)

            val cursor = database.query(
                    contract.tableName,
                    contract.columns,
                    where.clause(),
                    where.values(),
                    null,
                    null,
                    null)

            bindList(cursor, list)
        } catch (e: SQLException) {
            throw AndroidRuntimeException()
        } finally {
            close()
        }

        return list
    }

    @Synchronized
    open fun insert(model: M) {
        open(true)
        database.insert(contract.tableName, null, toContentValues(model))
        close()
    }

    @Synchronized
    open fun update(vararg columsValues: ColumnValue, where: Where) {
        val values = ContentValues()

        columsValues.map { values.put(it.column, it.value) }

        open(true)
        database.update(contract.tableName, values, where.toString(), arrayOf())
        close()
    }

    @Synchronized
    open fun delete(where: Where) {
        open(true)
        database.delete(contract.tableName, where.clause(), where.values())
        close()
    }

    @Synchronized
    open fun deleteAll() {
        open(true)
        database.delete(contract.tableName, null, null)
        close()
    }

    @Synchronized
    open protected fun bind(cursor: Cursor): M? {
        return if (!cursor.isBeforeFirst || cursor.moveToNext()) {
            toModel(cursor)
        } else {
            null
        }
    }

    open protected fun bindList(cursor: Cursor, list: ArrayList<M>) {
        while (cursor.moveToNext()) {
            list.add(bind(cursor)!!)
        }
    }

    abstract protected fun toModel(cursor: Cursor): M

    abstract protected fun toContentValues(model: M): ContentValues
}