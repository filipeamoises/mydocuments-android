package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

/**
 * Created by Filipe on 11/04/2018.
 */
open class DocumentFile(

        var id: Long? = null,
        var name: String? = null,

        @Expose(serialize = false)
        var type: FileType = FileType.NONE,

        @Expose(serialize = false, deserialize = false)
        var uriPath: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            FileType.getById(parcel.readInt()),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeInt(type.id)
        parcel.writeString(uriPath)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<DocumentFile> {

        override fun createFromParcel(parcel: Parcel): DocumentFile = DocumentFile(parcel)

        override fun newArray(size: Int): Array<DocumentFile?> = arrayOfNulls(size)
    }
}