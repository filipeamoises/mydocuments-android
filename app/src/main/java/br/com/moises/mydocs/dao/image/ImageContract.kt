package br.com.moises.mydocs.dao.image

import br.com.moises.mydocs.dao.BaseContract

/**
 * Created by Filipe on 10/04/2018.
 */
internal class ImageContract : BaseContract() {

    override val tableName = TABLE_NAME
    override val columns: Array<String> = Columns.ALL

    override fun createTable(): String {
        val sql = StringBuilder()

        sql.append("CREATE TABLE IF NOT EXISTS ${ImageContract.TABLE_NAME} (")
        sql.append("${Columns.ID} INTEGER DEFAULT NULL, ")
        sql.append("${Columns.USER_ID} INTEGER DEFAULT NULL, ")
        sql.append("${Columns.URI} TEXT DEFAULT NULL, ")
        sql.append("${Columns.HASH} TEXT DEFAULT NULL, ")
        sql.append("${Columns.UPLOADED} INTEGER NOT NULL DEFAULT 0")
        sql.append(");\n")
        sql.append("CREATE UNIQUE INDEX index_${Columns.ID} ON $tableName (${Columns.ID})")
        sql.append(";")

        println(sql)

        return sql.toString()
    }

    object Columns {

        val ID = "id"
        val USER_ID = "manifestation_id"
        val URI = "uri"
        val HASH = "hash"
        val UPLOADED = "uploaded"

        val ALL = arrayOf(ID, USER_ID, URI, HASH, UPLOADED)
    }

    companion object {

        val TABLE_NAME = "list"
    }
}
