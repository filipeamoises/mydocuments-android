package br.com.moises.mydocs.helper

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.webkit.MimeTypeMap
import br.com.moises.mydocs.R
import br.com.moises.mydocs.model.FileType
import br.com.moises.mydocs.service.FileService
import java.io.*
import java.security.MessageDigest

/**
 * Created by Filipe on 11/04/2018.
 */
object FileHelper {

    fun getFile(activity: AppCompatActivity,
                type: Type,
                requestCode: Int = 0,
                onFinished: (File) -> Unit = {}) {

        val title: String

        val items = when (type) {
            Type.IMAGE -> {
                title = activity.getString(R.string.lbl_add_image)
                arrayOf(
                        activity.getString(R.string.lbl_take_photo),
                        activity.getString(R.string.lbl_select_image),
                        activity.getString(R.string.lbl_cancel))
            }
            Type.VIDEO -> {
                title = activity.getString(R.string.lbl_add_video)
                arrayOf(
                        activity.getString(R.string.lbl_rec_video),
                        activity.getString(R.string.lbl_select_video),
                        activity.getString(R.string.lbl_cancel))
            }
            Type.AUDIO -> {
                title = activity.getString(R.string.lbl_add_audio)
                arrayOf(
                        activity.getString(R.string.lbl_rec_audio),
                        activity.getString(R.string.lbl_select_audio),
                        activity.getString(R.string.lbl_cancel))
            }
            Type.DOC -> {
                title = activity.getString(R.string.lbl_add_doc)
                arrayOf()
            }
        }

        when (type) {
            Type.DOC -> fromExplorer(activity, Type.DOC)
            else -> {
                val builder = AlertDialog.Builder(activity)

                builder.setTitle(title)
                builder.setItems(items,
                        { _, item ->
                            when (type) {
                                Type.IMAGE -> {
                                    when (item) {
                                        0 -> CameraHelper.takePic(activity, requestCode)
                                        1 -> fromGallery(activity, FileHelper.Type.IMAGE)
                                    }
                                }
                                Type.VIDEO -> {
                                    when (item) {
                                        0 -> CameraHelper.recVideo(activity, requestCode)
                                        1 -> fromGallery(activity, FileHelper.Type.VIDEO)
                                    }
                                }
                                Type.AUDIO -> {
                                    when (item) {
                                        0 -> AudioHelper.recAudio(activity, onFinished)
                                        1 -> fromExplorer(activity, Type.AUDIO)
                                    }
                                }
                                else -> {
                                    // Do nothing
                                }
                            }
                        })
                builder.show()
            }
        }
    }

    fun getFileDisplayName(contentURI: Uri): String {
        val cursor = ContextHelper.applicationContext.contentResolver.query(contentURI, null, null, null, null)

        return if (cursor == null) {
            getFileNameFromPath(contentURI.path)
        } else {
            cursor.moveToFirst()

            val colIndex = if (DocumentsContract.isDocumentUri(ContextHelper.applicationContext, contentURI)) {
                cursor.getColumnIndex(DocumentsContract.Document.COLUMN_DISPLAY_NAME)
            } else {
                cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME)
            }

            val displayName = cursor.getString(colIndex)

            cursor.close()

            displayName
        }
    }

    fun copyToPrivate(uri: Uri): Uri {
        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null

        return try {
            inputStream = BufferedInputStream(ContextHelper.applicationContext.contentResolver.openInputStream(uri))
            val filename = FileHelper.getFileDisplayName(uri)

            var target = File(ContextHelper.applicationContext.filesDir, filename)
            var tempFile = File(ContextHelper.applicationContext.filesDir, filename)

            // if file exists, change name
            var i = 0
            while (tempFile.exists()) {
                i += 1
                tempFile = File(ContextHelper.applicationContext.filesDir, "${target.nameWithoutExtension}_$i.${target.extension}")
            }
            target = tempFile

            outputStream = BufferedOutputStream(FileOutputStream(target))

            val buffer = ByteArray(1024)
            inputStream.read(buffer)
            do {
                outputStream.write(buffer)
            } while (inputStream.read(buffer) != -1)

            Uri.fromFile(target)
        } catch (e: IOException) {
            uri
        } finally {
            try {
                inputStream?.close()
                outputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun deleteFile(uriPath: String) {
        val file = File(uriPath)

        if (file.exists()) {
            val isInPrivate = uriPath.contains(ContextHelper.applicationContext.filesDir.path, true)

            if (isInPrivate) {
                file.delete()
            }
        }
    }

    fun fromGallery(activity: AppCompatActivity, type: Type = Type.DOC) {
        var intent = Intent(Intent.ACTION_PICK)

        intent.type = type.mimeType
        intent = Intent.createChooser(intent, activity.getString(R.string.lbl_select))

        startActivityForResult(activity, intent, type.requestCode, null)
    }

    fun fromExplorer(activity: AppCompatActivity, type: Type = Type.DOC) {
        var intent = Intent(Intent.ACTION_GET_CONTENT)

        intent.type = type.mimeType
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        intent = Intent.createChooser(intent, ContextHelper.applicationContext.getString(R.string.lbl_select))

        startActivityForResult(activity, intent, type.requestCode, null)
    }

    fun getMimeType(file: File): String {
        val extension = MimeTypeMap.getFileExtensionFromUrl(file.path)

        return if (extension.isNullOrEmpty()) {
            ""
        } else {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

            return mimeType ?: ""
        }
    }

    fun getModelFile(file: File): br.com.moises.mydocs.model.DocumentFile {
        val fileName = file.name
        val mimeType = FileHelper.getMimeType(file)
        val attachmentType = FileType.getByMimeType(mimeType)

        return br.com.moises.mydocs.model.DocumentFile(name = fileName, type = attachmentType, uriPath = Uri.fromFile(file).path)
    }

    fun getHash(uriPath: String): String {
        return try {
            val uri = Uri.parse(uriPath)
            val inputStream: InputStream = ContextHelper.applicationContext.contentResolver.openInputStream(uri)
            val md5Digest = MessageDigest.getInstance("MD5")
            val buffer = ByteArray(1024, { 0 })
            var numRead: Int

            do {
                numRead = inputStream.read(buffer)
                if (numRead > 0) {
                    md5Digest.update(buffer, 0, numRead)
                }
            } while (numRead != -1)

            inputStream.close()

            val md5 = md5Digest.digest()

            val result = StringBuffer()

            for (byte in md5) {
                result.append(String.format("%02X", byte))
            }

            result.toString()
        } catch (e: FileNotFoundException) {
            ""
        }
    }

    fun delete(paths: ArrayList<String>) {
        paths.map { deleteFile(it) }
    }

    fun deleteAsync(context: Context, paths: ArrayList<String>) {
        val intent = Intent(context, FileService::class.java)

        intent.putExtra(FileService.KEY_ACTION, FileService.KEY_ACTION_DELETE)
        intent.putExtra(FileService.KEY_URI_PATHS, paths)

        context.startService(intent)
    }

    fun copyToPrivateAsync(context: Context, uri: Uri) {
        val intent = Intent(context, FileService::class.java)

        intent.putExtra(FileService.KEY_ACTION, FileService.KEY_ACTION_COPY)
        intent.putExtra(FileService.KEY_URI, uri)

        context.startService(intent)
    }

    fun getFileNameFromPath(uriPath: String): String {
        return try {
            val arr = uriPath.split("/")

            arr[arr.size - 1]
        } catch (e: Exception) {
            ""
        }
    }

    enum class Type(val mimeType: String, val requestCode: Int) {
        DOC("*/*",3000),
        IMAGE("image/*",2000),
        AUDIO("audio/*",5000),
        VIDEO("video/*",1000)
    }
}