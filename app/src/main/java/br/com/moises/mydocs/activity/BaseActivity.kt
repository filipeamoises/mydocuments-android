package br.com.moises.mydocs.activity

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.ViewGroup
import br.com.moises.mydocs.R
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.DialogHelper
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.view.IView


/**
 * Base of ALL activities of this project
 *
 * @author Filipe Moises
 * @param layout the layout resource.
 * @param title the title resource.
 */
abstract class BaseActivity<V : IView>(var layoutRes: Int, private val titleRes: Int = R.string.lbl_empty) : AppCompatActivity() {


    lateinit var rootView: V

    lateinit var dialog: DialogHelper.Builder

    private var receiver = Receiver()

    var toolbarTitle: Int = R.string.app_name
        set(value) {
            field = value
            rootView.toolbarTitle.text = getString(value)
        }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = Configuration.ORIENTATION_PORTRAIT

        setContentView(layoutRes)

        val content: ViewGroup = findViewById(android.R.id.content) as ViewGroup

        rootView = content.getChildAt(0) as V

        toolbarTitle = titleRes

        setSupportActionBar(rootView.toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)

        dialog = DialogHelper.Builder(this)
    }

    override fun onResume() {
        val intentFilter = BaseBusiness.buildErrorIntentFilter(this::class.java.canonicalName!!)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).registerReceiver(receiver, intentFilter)

        super.onResume()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).unregisterReceiver(receiver)

        super.onPause()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    open fun showMessage(message: String) {
        dialog.message(message)
        dialog.show()
    }

    fun showLoading() {
        rootView.showLoading()
    }

    fun hideLoading() {
        rootView.hideLoading()
    }


    open fun onRestError(response: ProblemApiResponse) {
        val msg = when {
            response.code == "401" -> {
                dialog.listener {
                    showLoading()
//                    startService(LogoutBusiness.makeIntent(this::class))
                    ActivityHelper.startActivityFinishingPrevious(InitActivity::class.java)
                }
                dialog.title(getString(R.string.err_authentication_title))
                getString(R.string.err_authentication)
            }
            response.detail.isNullOrEmpty() -> getString(R.string.msg_local_error)
            else -> response.detail!!
        }

        hideLoading()

        dialog.message(msg)
        dialog.show()
    }

    open fun onRestFailure(response: String) {
        hideLoading()

        dialog.message(response)
        dialog.show()
    }

    inner class Receiver : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            when (action) {
                BaseBusiness.PROBLEM_API_RESPONSE_INTENT_ACTION -> {
                    onRestError(bundle.getParcelable(BaseBusiness.REST_ERROR_ITEM))
                }
                BaseBusiness.FAILURE_INTENT_ACTION -> {
                    onRestFailure(getString(R.string.msg_local_error))
                }
            }
        }
    }
}