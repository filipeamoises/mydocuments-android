package br.com.moises.mydocs.business.document

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Environment
import android.support.v4.app.NotificationCompat
import android.widget.Toast
import br.com.moises.mydocs.R
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.Download
import br.com.moises.mydocs.model.Entries
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.rest.document.DocumentRestApi
import okhttp3.ResponseBody
import retrofit2.Retrofit
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.reflect.KClass

/**
 * Created by Filipe on 11/04/2018.
 */
class DocumentDownloadBusiness() : BaseBusiness<Download>(ACTION_SUCCESS, TAG) {

    private var notificationBuilder: NotificationCompat.Builder? = null
    private var notificationManager: NotificationManager? = null
    private var totalFileSize: Int = 0
    private lateinit var file: Entries

    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)
        file = intent!!.getParcelableExtra<Entries>(INTENT_REQUEST)
        startService()
    }

    private fun startService() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_cloud_download_black_24dp)
                .setContentTitle(getString(R.string.lbl_download))
                .setContentText(getString(R.string.lbl_downloading))
                .setAutoCancel(true)
        notificationManager!!.notify(0, notificationBuilder!!.build())

        initDownload()
    }

    private fun initDownload() {

        val retrofit = Retrofit.Builder()
                .baseUrl(ContextHelper.getHttpHost())
                .build()

        val retrofitInterface = retrofit.create(DocumentRestApi::class.java)

        val request = retrofitInterface.download(PreferencesHelper.getInt(PreferenceKeys.USER_ID), file.name)
        try {

            downloadFile(request.execute().body())

        } catch (e: IOException) {

            e.printStackTrace()
            Toast.makeText(applicationContext, e.message, Toast.LENGTH_SHORT).show()

        }
    }

    @Throws(IOException::class)
    private fun downloadFile(body: ResponseBody?) {

        var count: Int = 0
        val data = ByteArray(1024 * 4)
        val fileSize = body!!.contentLength()
        val bis = BufferedInputStream(body.byteStream(), 1024 * 8)
//        Create a directory for storage local files
        val folder = File(Environment.getExternalStorageDirectory(), getString(R.string.directory_name))
        if (!folder.exists()) {
            folder.mkdirs()
        }

        val outputFile = File(folder, file.name)
        val output = FileOutputStream(outputFile)
        var total: Long = 0
        val startTime = System.currentTimeMillis()
        var timeCount = 1

        while (true) {
            total += count.toLong()
            totalFileSize = (fileSize / Math.pow(1024.0, 2.0)).toInt()
            val current = Math.round(total / Math.pow(1024.0, 2.0)).toDouble()

            val progress = (total * 100 / fileSize).toInt()

            val currentTime = System.currentTimeMillis() - startTime

            val download = Download()
            download.totalFileSize = totalFileSize

            if (currentTime > 1000 * timeCount) {

                download.currentFileSize = current.toInt()
                download.progress = progress
                sendNotification(download)
                timeCount++
            }

            output.write(data, 0, count)
            count = bis.read(data)
            if (count == -1)
                break
        }
        onDownloadComplete()
        output.flush()
        output.close()
        bis.close()

    }

    private fun sendNotification(download: Download) {
        notificationBuilder!!.setProgress(100, download.progress, false)
        notificationBuilder!!.setContentText("Downloading file " + download.currentFileSize + "/" + totalFileSize + " MB")
        notificationManager!!.notify(0, notificationBuilder!!.build())
    }

    private fun onDownloadComplete() {

        val download = Download()
        download.progress = 100
        onRestSuccess(download)

        notificationManager!!.cancel(0)
        notificationBuilder!!.setProgress(0, 0, false)
        notificationBuilder!!.setContentText("File Downloaded")
        notificationManager!!.notify(0, notificationBuilder!!.build())

    }

    override fun onTaskRemoved(rootIntent: Intent) {
        notificationManager!!.cancel(0)
    }

    override fun onRestSuccess(response: Download?) {
        super.onRestSuccess(response)
    }

    companion object {

        private val CLASS = DocumentDownloadBusiness::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_document_download_success"
        public val INTENT_REQUEST: String = "intent_request_download_success"


        fun makeIntent(classCaller: KClass<*>): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)

            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>) {
            BaseBusiness.registerReceiver(receiver, callerClass, ACTION_SUCCESS)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            BaseBusiness.removeReceiver(receiver)
        }
    }
}