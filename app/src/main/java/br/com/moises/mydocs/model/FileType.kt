package br.com.moises.mydocs.model

/**
 * Created by Filipe on 11/04/2018.
 */

enum class FileType(val id: Int) {

    NONE(0),
    DOC(1),
    IMAGE(2),
    AUDIO(3),
    VIDEO(4),
    LOCATION(5);

    companion object {

        fun getById(id: Int): FileType {
            val items = FileType.values()
            return items.firstOrNull { it.id == id } ?: FileType.NONE
        }


        fun getByMimeType(mimeType: String): FileType = when {
            mimeType.contains("image") -> IMAGE
            mimeType.contains("video") -> VIDEO
            mimeType.contains("audio") -> AUDIO
            else -> {
                DOC
            }
        }
    }
}