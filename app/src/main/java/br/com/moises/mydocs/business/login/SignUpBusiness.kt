package br.com.moises.mydocs.business.login

import android.content.BroadcastReceiver
import android.content.Intent
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.SignUp
import br.com.moises.mydocs.rest.login.SignUpRestService
import kotlin.reflect.KClass

/**
 * Created by Filipe on 12/04/2018.
 */
class SignUpBusiness : BaseBusiness<Auth>(ACTION_SUCCESS, TAG) {
    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)

        val signUp = intent!!.getParcelableExtra<SignUp>(KEY_SIGNUP)
        callService(signUp)
    }

    private fun callService(signUp: SignUp) {
        val service = SignUpRestService(signUp, this)

        service.start()
    }

    override fun onRestSuccess(response: Auth?) {
        super.onRestSuccess(response)
    }

    companion object {

        private val CLASS = SignUpBusiness::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_signup_success"

        private val KEY_SIGNUP = "key_signup"

        fun makeIntent(classCaller: KClass<*>, signUp: SignUp): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)

            intent.putExtra(KEY_SIGNUP, signUp)

            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>) {
            BaseBusiness.registerReceiver(receiver, callerClass, ACTION_SUCCESS)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            BaseBusiness.removeReceiver(receiver)
        }

    }
}