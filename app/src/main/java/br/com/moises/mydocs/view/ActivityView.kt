package br.com.moises.mydocs.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.moises.mydocs.R

/**
 * Created by Filipe on 11/04/2018.
 */
open class ActivityView : ConstraintLayout, IView {

    override lateinit var appbar: AppBarLayout

    override lateinit var toolbar: Toolbar

    override lateinit var toolbarTitle: TextView

    override lateinit var contentView: ConstraintLayout

    override lateinit var loadingView: ConstraintLayout

    private var layoutInflated: Boolean = false

    private var isFinishInflate: Boolean = false

    protected var isLoading: Boolean = false
        set(value) {
            field = value
            loadingView.visibility = if (value) View.VISIBLE else View.GONE
        }

    constructor(context: Context) : super(context) {
        config()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        config()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        config()
    }

    protected fun config() {
        if (!isInEditMode) {
            LayoutInflater.from(context).inflate(R.layout.view_activity, this, true)

            appbar = findViewById(R.id.activity_appbar)
            toolbar = findViewById(R.id.activity_toolbar)
            toolbarTitle = toolbar.findViewById(R.id.activity_toolbar_title)
            loadingView = findViewById(R.id.activity_loading)
            contentView = findViewById(R.id.activity_content)

            hideLoading()
        }
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (layoutInflated) {
            contentView.addView(child, index, params)
        } else {
            super.addView(child, index, params)
            layoutInflated = true
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        isFinishInflate = true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean = isLoading

    override fun showLoading() {
        isLoading = true
    }

    override fun hideLoading() {
        isLoading = false
    }
}