package br.com.moises.mydocs.rest.document

import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.rest.BaseRestService
import br.com.moises.mydocs.rest.JsonRestReponseCallback
import okhttp3.ResponseBody

/**
 * Created by Filipe on 11/04/2018.
 */
open class DocumentDownloadRestService(private val fileName: String, callback: JsonRestReponseCallback<ResponseBody>) : BaseRestService<Long, ResponseBody, DocumentRestApi>(callback) {

    override val apiClass: Class<DocumentRestApi>
        get() = DocumentRestApi::class.java

    override fun create(): Int = 0

    public override fun execute(documentRestApi: DocumentRestApi) {
        super.enqueue(documentRestApi.download(PreferencesHelper.getInt(PreferenceKeys.USER_ID),fileName))
    }

    public override fun onRestSuccess(tokenResponse: ResponseBody?) {
        super.onRestSuccess(tokenResponse)
    }

    public override fun onRestError(response: ProblemApiResponse) {
        super.onRestError(response)
    }

    public override fun onFailure(t: Throwable) {
        super.onFailure(t)
    }
}