package br.com.moises.mydocs.rest.document

import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.rest.BaseRestService
import br.com.moises.mydocs.rest.JsonRestReponseCallback
import okhttp3.MultipartBody

/**
 * Created by Filipe on 12/04/2018.
 */
open class DocumentUploadRestService(private val fileUri : MultipartBody.Part, callback: JsonRestReponseCallback<Void>) : BaseRestService<Long, Void, DocumentRestApi>(callback) {

    override val apiClass: Class<DocumentRestApi>
        get() = DocumentRestApi::class.java

    override fun create(): Int = 0

    public override fun execute(restApi: DocumentRestApi) {
        super.enqueue(restApi.uploadFile(fileUri, PreferencesHelper.getInt(PreferenceKeys.USER_ID)))
    }

    public override fun onRestError(response: ProblemApiResponse) {
        super.onRestError(response)
    }

    public override fun onFailure(t: Throwable) {
        super.onFailure(t)
    }
}