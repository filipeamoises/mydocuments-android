package br.com.moises.mydocs.dao

/**
 * Created by Filipe on 10/04/2018.
 */
internal abstract class BaseContract {

    abstract val tableName: String
    abstract val columns: Array<String>

    abstract fun createTable(): String

    companion object {

        val NULL = "NULL"
        val TRUE = "1"
        val FALSE = "0"
    }
}