package br.com.moises.mydocs.helper

import android.content.Context
import br.com.moises.mydocs.R

/**
 * Created by Filipe on 10/04/2018.
 */
object ContextHelper {

    private var _hostProtocol: String? = null
    private var _hostDomain: String? = null
    private var _hostPort: String? = null
    private var _hostContext: String? = null

    lateinit var applicationContext: Context
        private set

    fun init(context: Context) {
        applicationContext = context
    }

    var hostProtocol: String
        get() {
            if (_hostProtocol == null) {
                _hostProtocol = applicationContext.getString(R.string.host_protocol)
            }
            return _hostProtocol ?: ""
        }
        set(value) {
            _hostProtocol = value
        }

    var hostDomain: String
        get() {
            if (_hostDomain == null) {
                _hostDomain = applicationContext.getString(R.string.host_domain)
            }
            return _hostDomain ?: ""
        }
        set(value) {
            _hostDomain = value
        }

    var hostPort: String
        get() {
            if (_hostPort == null) {
                _hostPort = applicationContext.getString(R.string.host_port)
            }
            return _hostPort ?: ""
        }
        set(value) {
            _hostPort = value
        }

    var hostContext: String
        get() {
            if (_hostContext == null) {
                _hostContext = applicationContext.getString(R.string.host_context)
            }
            return _hostContext ?: ""
        }
        set(value) {
            _hostContext = value
        }

    var msgErrorRestNaoIdentificado: String = "Erro"
        get() = applicationContext.getString(R.string.msg_local_error)

     fun getHttpHost(): String = "${hostProtocol}://${hostDomain}:${hostPort}${hostContext}"
}
