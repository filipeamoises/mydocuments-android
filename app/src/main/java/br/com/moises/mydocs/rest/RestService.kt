package br.com.moises.mydocs.rest

import android.widget.Toast
import br.com.moises.mydocs.activity.LoginActivity
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.util.DateUtil
import br.com.moises.mydocs.util.RootUnwrappedParserUtil
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*


/**
 * Created by Filipe on 11/04/2018.
 */
abstract class RestService<R, C, A>(private val tceRestReponseCallback: RestReponseCallback<C>?) : Callback<C> {

    companion object {
        private val KEY_TOKEN_HEADER = "x-access-token"

    }

    protected abstract fun getDomainPath(): String

    protected abstract fun create(): Int

    protected abstract val apiClass: Class<A>

    fun start() {
        try {

            val gson = GsonBuilder().setLenient()
                    .registerTypeAdapter(Date::class.java, object : TypeAdapter<Date?>() {
                        override fun read(reader: JsonReader?): Date? {
                            if (reader == null) {
                                return null
                            }

                            if (reader.peek() == JsonToken.NULL) {
                                reader.nextNull()
                                return null
                            }

                            val strDate = reader.nextString()
                            return DateUtil.toDate(strDate)
                        }

                        override fun write(writer: JsonWriter?, value: Date?) {
                            if (writer == null) {
                                return
                            }

                            if (value == null) {
                                writer.nullValue()
                                return
                            }

                            val strDate = DateUtil.toString(value)
                            writer.value(strDate)
                        }

                    }).create()

            val client = OkHttpClient.Builder()

            client.addInterceptor { chain ->
                val original = chain.request()

                var request = original.newBuilder()
                        .method(original!!.method(), original.body())
                        .build()

                val token = PreferencesHelper.getString(PreferenceKeys.APP_SESSION_TOKEN)
                if (token.isNotEmpty()) {
                    request = request.newBuilder()
                            .addHeader(KEY_TOKEN_HEADER, token)
                            .build()
                }

                chain.proceed(request)
            }

            client.addInterceptor { chain ->
                val response = chain.proceed(chain.request())
                response
            }

            val retrofit = Retrofit.Builder()
                    .baseUrl(getDomainPath())
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

            val apiClass = retrofit.create(apiClass)

            execute(apiClass)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    protected fun enqueue(executionToEnqueue: Call<C>) {
        executionToEnqueue.enqueue(this)
    }

    protected abstract fun execute(apiClass: A)

    override fun onFailure(call: Call<C>, t: Throwable) {
        onFailure(t)
    }

    override fun onResponse(call: Call<C>, response: Response<C>) {
        if (response.isSuccessful) {
            onRestSuccess(response.body())
        } else if(response.code() == 401) {
            Toast.makeText(ContextHelper.applicationContext,"Ocorreu um erro de autenticação. Faça o login novamente.", Toast.LENGTH_SHORT).show()
            PreferencesHelper.reset()
            ActivityHelper.startActivityFinishingPrevious(LoginActivity::class.java)
        } else {
            try {
                val errorStream = response.errorBody()!!.bytes()
                if (errorStream.isNotEmpty()) {
                    onRestError(rootUnwrappedParser.parse(ByteArrayInputStream(errorStream)))
                } else {
                    onRestError(buildProblemApiResponseFromEmptyResult())
                }
            } catch (e: JsonSyntaxException) {
                onFailure(e)
            } catch (e: IOException) {
                onFailure(e)
            }
        }
    }

    private fun buildProblemApiResponseFromEmptyResult() =
            ProblemApiResponse(
                    title = ContextHelper.msgErrorRestNaoIdentificado,
                    detail = ContextHelper.msgErrorRestNaoIdentificado,
                    code = "500")

    private val rootUnwrappedParser: RootUnwrappedParserUtil<ProblemApiResponse>
        get() = object : RootUnwrappedParserUtil<ProblemApiResponse>() {

            @Throws(JsonSyntaxException::class, IOException::class)
            override fun parse(jsonContent: InputStream): ProblemApiResponse {
                return getObjectMapper().fromJson(InputStreamReader(jsonContent), ProblemApiResponse::class.java)
            }

            @Throws(JsonSyntaxException::class, IOException::class)
            override fun parse(jsonContent: String): ProblemApiResponse {
                return getObjectMapper().fromJson(jsonContent, ProblemApiResponse::class.java)
            }

            @Throws(JsonSyntaxException::class, IOException::class)
            override fun toJson(tceS3ObjectList: ProblemApiResponse): ByteArray {
                return getObjectMapper().toJson(tceS3ObjectList).toByteArray()
            }

        }

    protected open fun onRestSuccess(responseBody: C?) {
        if (this.tceRestReponseCallback != null) {
            if (this.tceRestReponseCallback is InputStreamRestReponseCallback) {
                try {
                    (this.tceRestReponseCallback as InputStreamRestReponseCallback).onRestSuccess((responseBody as ResponseBody).bytes())
                } catch (e: IOException) {
                    onFailure(e)
                }

            } else if (this.tceRestReponseCallback is JsonRestReponseCallback<*>) {
                (this.tceRestReponseCallback as JsonRestReponseCallback<C>).onRestSuccess(responseBody)
            }
        }
    }

    protected open fun onRestError(response: ProblemApiResponse) {
        if (this.tceRestReponseCallback != null) {
            this.tceRestReponseCallback.onRestError(response)
        }
    }

    protected open fun onFailure(t: Throwable) {
        if (this.tceRestReponseCallback != null) {
            this.tceRestReponseCallback.onFailure(t)
        }
    }
}
