package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class ContentSettings(
        @Expose
        @SerializedName("contentType")
        val contenttype: String,
        @Expose
        @SerializedName("contentEncoding")
        val contentencoding: String,
        @Expose
        @SerializedName("contentLanguage")
        val contentlanguage: String,
        @Expose
        @SerializedName("contentMD5")
        val contentmd5: String,
        @Expose
        @SerializedName("cacheControl")
        val cachecontrol: String,
        @Expose
        @SerializedName("contentDisposition")
        val contentdisposition: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(contenttype)
        parcel.writeString(contentencoding)
        parcel.writeString(contentlanguage)
        parcel.writeString(contentmd5)
        parcel.writeString(cachecontrol)
        parcel.writeString(contentdisposition)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContentSettings> {
        override fun createFromParcel(parcel: Parcel): ContentSettings {
            return ContentSettings(parcel)
        }

        override fun newArray(size: Int): Array<ContentSettings?> {
            return arrayOfNulls(size)
        }
    }
}