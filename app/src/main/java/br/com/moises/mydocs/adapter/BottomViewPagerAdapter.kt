package br.com.moises.mydocs.adapter

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import br.com.moises.mydocs.fragment.BaseFragment


/**
 * Created by Filipe on 11/04/2018.
 */

class BottomViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = ArrayList<BaseFragment>()
    /**
     * Get the current fragment
     */
    var currentFragment: BaseFragment? = null
        private set

    fun add(frag: BaseFragment) {
        this.fragments.add(frag)

    }

    override fun getItem(position: Int): BaseFragment {
        return fragments.get(position)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        if (currentFragment !== `object`) {
            currentFragment = `object` as BaseFragment
        }
        super.setPrimaryItem(container, position, `object`)
    }

    override fun getCount(): Int {
       return fragments.size
    }

}