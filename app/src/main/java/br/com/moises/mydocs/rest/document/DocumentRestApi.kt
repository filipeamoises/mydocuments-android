package br.com.moises.mydocs.rest.document

import br.com.moises.mydocs.model.Entries
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Filipe on 11/04/2018.
 */
interface DocumentRestApi {


//    @Headers("Content-type: application/json")
//    @PUT("document/status")
//    fun statusList(): Call<List<Status>>
    @Streaming
    @Headers("Content-type: application/json")
    @GET("file/download/{userId}/{fileName}")
    fun download(@Path("userId") userId: Int, @Path("fileName") fileName: String): Call<ResponseBody>

    @Headers("Content-type: application/json")
    @GET("file/list/{userId}")
    fun list(@Path("userId") userId: Int): Call<List<Entries>>

    @Multipart
    @POST("file/upload")
    fun uploadFile(@Part file: MultipartBody.Part, @Header("userId") userId: Int) : Call<Void>
}