package br.com.moises.mydocs.helper

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import br.com.moises.mydocs.R
import com.afollestad.materialcamera.MaterialCamera
import java.io.File

/**
 * Created by Filipe on 10/04/2018.

* val VIDEO_REQUEST_CODE = 1000
* val PHOTO_REQUEST_CODE = 2000
*
* CameraHelper.takePic(this, PHOTO_REQUEST_CODE)
* CameraHelper.recVideo(this, VIDEO_REQUEST_CODE)
*
* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
    *   super.onActivityResult(requestCode, resultCode, data)
    *     if (resultCode == Activity.RESULT_OK) {
        *       when (requestCode) {
                *         PHOTO_REQUEST_CODE -> {
                *           println(FileHelper.getFile(data))
                *         }
                *         VIDEO_REQUEST_CODE -> {
                *           println(FileHelper.getFile(data))
                *         }
                *     } else {
            *       when (requestCode) {
                *         PHOTO_REQUEST_CODE -> {
                *           println(CameraHelper.getException(data))
                *       }
                *       VIDEO_REQUEST_CODE -> {
                *         println(CameraHelper.getException(data))
                *       }
                *     }
            *   }
        * }
    */
    object CameraHelper {

        val MAIN_DIR = "camera"
        val PHOTO_DIR = "${MAIN_DIR}/photo"
        val VIDEO_DIR = "${MAIN_DIR}/video"

        fun takePic(activity: AppCompatActivity, requestCode: Int) {
            val directory = File(ContextHelper.applicationContext.filesDir, PHOTO_DIR)

            MaterialCamera(activity)
                    .saveDir(directory)
                    .stillShot()
                    .start(requestCode)
        }

        fun recVideo(activity: AppCompatActivity, requestCode: Int) {
            val directory = File(ContextHelper.applicationContext.filesDir, VIDEO_DIR)

            MaterialCamera(activity)
                    .allowRetry(true)                                  // Whether or not 'Retry' is visible during playback
                    .autoSubmit(false)                                 // Whether or not user is allowed to playback videos after recording. This can affect other things, discussed in the next section.
                    .saveDir(directory)                                // The folder recorded videos are saved to
                    .primaryColorAttr(R.attr.colorPrimary)             // The theme color used for the camera, defaults to colorPrimary of Activity in the constructor
                    .showPortraitWarning(true)                         // Whether or not a warning is displayed if the user presses record in portrait orientation
                    .defaultToFrontFacing(false)                       // Whether or not the camera will initially show the front facing camera
                    .retryExits(false)                                 // If true, the 'Retry' button in the playback screen will exit the camera instead of going back to the recorder
                    .restartTimerOnRetry(false)                        // If true, the countdown timer is reset to 0 when the user taps 'Retry' in playback
                    .continueTimerInPlayback(false)                    // If true, the countdown timer will continue to go down during playback, rather than pausing.
                    .qualityProfile(MaterialCamera.QUALITY_HIGH)       // Sets a quality profile, manually setting bit rates or frame rates with other settings will overwrite individual quality profile settings
                    .videoPreferredHeight(720)                         // Sets a preferred height for the recorded video output.
                    .videoPreferredAspect(4f / 3f)                     // Sets a preferred aspect ratio for the recorded video output.
                    .iconRecord(R.drawable.mcam_action_capture)        // Sets a custom icon for the button used to start recording
                    .iconStop(R.drawable.mcam_action_stop)             // Sets a custom icon for the button used to stop recording
                    .iconFrontCamera(R.drawable.mcam_camera_front)     // Sets a custom icon for the button used to switch to the front camera
                    .iconRearCamera(R.drawable.mcam_camera_rear)       // Sets a custom icon for the button used to switch to the rear camera
                    .iconPlay(R.drawable.evp_action_play)              // Sets a custom icon used to start playback
                    .iconPause(R.drawable.evp_action_pause)            // Sets a custom icon used to pause playback
                    .iconRestart(R.drawable.evp_action_restart)        // Sets a custom icon used to restart playback
                    .labelRetry(R.string.mcam_retry)                   // Sets a custom button label for the button used to retry recording, when available
                    .labelConfirm(R.string.mcam_use_video)             // Sets a custom button label for the button used to confirm/submit a recording
                    .start(requestCode)                                // Starts the camera activity, the result will be sent back to the current Activity
        }

        fun getException(data: Intent): Exception = data.getSerializableExtra(MaterialCamera.ERROR_EXTRA) as Exception
    }