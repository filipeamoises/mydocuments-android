package br.com.moises.mydocs.rest.document

import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Environment
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast
import br.com.moises.mydocs.R
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.Download
import br.com.moises.mydocs.model.PreferenceKeys
import okhttp3.ResponseBody
import retrofit2.Retrofit
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.reflect.KClass

/**
 * Created by Filipe on 17/04/2018.
 */

class DownloadService : IntentService("Download Service") {

    private var notificationBuilder: NotificationCompat.Builder? = null
    private var notificationManager: NotificationManager? = null
    private var totalFileSize: Int = 0
    companion object {


        private val CLASS = DownloadService::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_document_download_success"
        public val INTENT_REQUEST: String = "intent_request_download_success"


        fun makeIntent(classCaller: KClass<*>): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)

            return intent
        }
    }

    override fun onHandleIntent(intent: Intent?) {

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_cloud_download_black_24dp)
                .setContentTitle(getString(R.string.lbl_download))
                .setContentText(getString(R.string.lbl_downloading))
                .setAutoCancel(true)
        notificationManager!!.notify(0, notificationBuilder!!.build())

        initDownload(intent!!.getStringExtra(INTENT_REQUEST))

    }

    private fun initDownload(fileName: String) {

        val retrofit = Retrofit.Builder()
                .baseUrl(ContextHelper.getHttpHost())
                .build()

        val retrofitInterface = retrofit.create(DocumentRestApi::class.java)

        val request = retrofitInterface.download(PreferencesHelper.getInt(PreferenceKeys.USER_ID),fileName )
        try {

            downloadFile(request.execute().body())

        } catch (e: IOException) {

            e.printStackTrace()
            Toast.makeText(applicationContext, e.message, Toast.LENGTH_SHORT).show()

        }

    }

    @Throws(IOException::class)
    private fun downloadFile(body: ResponseBody?) {

        var count: Int
        val data = ByteArray(1024 * 4)
        val fileSize = body!!.contentLength()
        val bis = BufferedInputStream(body.byteStream(), 1024 * 8)
        val outputFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "file.zip")
        val output = FileOutputStream(outputFile)
        var total: Long = 0
        val startTime = System.currentTimeMillis()
        var timeCount = 1
        count = bis.read(data)
        while (count != -1) {
            count = bis.read(data)
            total += count.toLong()
            totalFileSize = (fileSize / Math.pow(1024.0, 2.0)).toInt()
            val current = Math.round(total / Math.pow(1024.0, 2.0)).toDouble()

            val progress = (total * 100 / fileSize).toInt()

            val currentTime = System.currentTimeMillis() - startTime

            val download = Download()
            download.totalFileSize = totalFileSize

            if (currentTime > 1000 * timeCount) {

                download.currentFileSize = current.toInt()
                download.progress = progress
                sendNotification(download)
                timeCount++
            }

            output.write(data, 0, count)
        }
        onDownloadComplete()
        output.flush()
        output.close()
        bis.close()

    }

    private fun sendNotification(download: Download) {

        sendIntent(download)
        notificationBuilder!!.setProgress(100, download.progress, false)
        notificationBuilder!!.setContentText("Downloading file " + download.currentFileSize + "/" + totalFileSize + " MB")
        notificationManager!!.notify(0, notificationBuilder!!.build())
    }

    private fun sendIntent(download: Download) {

        val intent = Intent("")
        intent.putExtra("download", download)
        LocalBroadcastManager.getInstance(this@DownloadService).sendBroadcast(intent)
    }

    private fun onDownloadComplete() {

        val download = Download()
        download.progress = 100
        sendIntent(download)

        notificationManager!!.cancel(0)
        notificationBuilder!!.setProgress(0, 0, false)
        notificationBuilder!!.setContentText("File Downloaded")
        notificationManager!!.notify(0, notificationBuilder!!.build())

    }

    override fun onTaskRemoved(rootIntent: Intent) {
        notificationManager!!.cancel(0)
    }

}