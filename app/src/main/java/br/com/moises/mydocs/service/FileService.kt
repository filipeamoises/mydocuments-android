package br.com.moises.mydocs.service

import android.app.IntentService
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.support.v4.content.LocalBroadcastManager
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.FileHelper
import br.com.moises.mydocs.model.DocumentFile
import br.com.moises.mydocs.model.FileType
import br.com.moises.mydocs.util.toFile

/**
 * Created by Filipe on 11/04/2018.
 */
class FileService : IntentService("FileService") {

    override fun onHandleIntent(intent: Intent?) {
        val action = intent!!.extras.get(KEY_ACTION)

        when (action) {
            KEY_ACTION_DELETE -> {
                val paths = intent.extras.getStringArrayList(KEY_URI_PATHS)

                delete(paths)
            }
            KEY_ACTION_COPY -> {
                val uri = intent.extras.getParcelable<Uri>(KEY_URI)

                copyToPrivate(uri)
            }
            else -> {
                // Do nothing
            }
        }
    }

    private fun copyToPrivate(uri: Uri) {
        val newUri = FileHelper.copyToPrivate(uri)

        // if original file is already from private, delete original
        val isInPrivate = uri.path.contains(ContextHelper.applicationContext.filesDir.path, true)
        if (isInPrivate) {
            uri.toFile().delete()
        }

        val newFile = newUri.toFile()
        val fileName = FileHelper.getFileDisplayName(newUri)
        val mimeType = FileHelper.getMimeType(newFile)

        val file = DocumentFile(
                name = fileName,
                uriPath = newUri.path,
                type = FileType.getByMimeType(mimeType))

        val intent = Intent(RESPONSE_FILE_COPY_SUCCESSFUL)
        intent.putExtra(KEY_MODEL_FILE, file)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).sendBroadcast(intent)
    }

    private fun delete(paths: ArrayList<String>) {
        FileHelper.delete(paths)

        // Return always successful
        val intent = Intent(RESPONSE_FILE_DELETE_SUCCESSFUL)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).sendBroadcast(intent)
    }

    companion object {

        val KEY_ACTION = "action"

        val KEY_ACTION_DELETE = "action_delete"
        val KEY_ACTION_COPY = "action_copy"
        val KEY_URI = "file_path"
        val KEY_URI_PATHS = "uri_paths"
        val KEY_MODEL_FILE = "model_file"

        val RESPONSE_FILE_COPY_SUCCESSFUL = "response_file_copy_successful"
        val RESPONSE_FILE_DELETE_SUCCESSFUL = "response_file_delete_successful"

        fun makeIntentFilter(): IntentFilter {
            val filter = IntentFilter()

            filter.addAction(RESPONSE_FILE_COPY_SUCCESSFUL)
            filter.addAction(RESPONSE_FILE_DELETE_SUCCESSFUL)

            return filter
        }
    }
}