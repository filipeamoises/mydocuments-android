package br.com.moises.mydocs.util

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Filipe on 11/04/2018.
 */
fun Date.toFormattedDate(format: String = "dd MMM yyyy"): String {
    val formatter = SimpleDateFormat(format)

    return try {
        return formatter.format(this)
    } catch (e: Exception) {
        ""
    }
}

@SuppressLint("SimpleDateFormat")
abstract class DateUtil private constructor() {
    companion object {

        val LOCALE_PT_BR = Locale("pt", "BR")
        val SIMPLE_DATETIME_FORMAT_COMPRESSED = SimpleDateFormat("yyyyMMddHHmmss", LOCALE_PT_BR)
        val SIMPLE_DATETIME_FORMAT_PT_BR = SimpleDateFormat("dd/MM/yyyy", LOCALE_PT_BR)

        private var mapShortDaysWeek: MutableMap<Int, String>? = null

        init {
            val displayNames = calendar.getDisplayNames(Calendar.DAY_OF_WEEK, Calendar.SHORT, LOCALE_PT_BR)
            mapShortDaysWeek = HashMap()
            for ((key, value) in displayNames) {
                mapShortDaysWeek!!.put(value, key)
            }
        }

        fun isFirtsCalendarAfterSecondCalendar(firstCalendar: Calendar, secondCalendar: Calendar): Boolean {
            return firstCalendar.compareTo(secondCalendar) == 1
        }

        fun isFirtsDateAfterSecondDate(firstDate: Date, secondDate: Date): Boolean {
            return firstDate.compareTo(secondDate) > 0
        }

        val emptyCalendar: Calendar
            get() {
                val calendar = calendar
                calendar.set(Calendar.YEAR, 0)
                calendar.set(Calendar.MONTH, 0)
                calendar.set(Calendar.DAY_OF_MONTH, 0)
                calendar.set(Calendar.HOUR_OF_DAY, 0)
                calendar.set(Calendar.HOUR, 0)
                calendar.set(Calendar.MINUTE, 0)
                calendar.set(Calendar.SECOND, 0)
                calendar.set(Calendar.MILLISECOND, 0)
                return calendar
            }

        fun dateToBaseYearMonthAndDayOfMonth(date: Date): Calendar {
            val calendar = calendar
            calendar.time = date
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.HOUR, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar
        }

        fun dateToCalendar(date: Date): Calendar {
            val calendar = calendar
            calendar.time = date
            return calendar
        }

        fun yearMonthAndDayOfMonthToCalendar(year: Int, month: Int, dayOfMonth: Int): Calendar {
            val calendar = emptyCalendar
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            return calendar
        }

        val calendar: Calendar
            get() = Calendar.getInstance(LOCALE_PT_BR)

        fun toSimpleText(date: Date): String {
            return SimpleDateFormat("dd/MM/yyyy", LOCALE_PT_BR).format(date)
        }

        fun toSimpleDayText(date: Date): String {
            val calendar = calendar
            calendar.time = date
            var day: String? = calendar.getDisplayName(java.util.Calendar.DAY_OF_WEEK, Calendar.LONG, LOCALE_PT_BR)
            if (day!!.contains("-")) {
                day = day.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
            }
            return day.toUpperCase(LOCALE_PT_BR)
        }

        fun getMonthAbbreviated(mes: Int): String {
            val meses = arrayOf("JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ")
            return meses[mes]
        }

        fun toSimpleHourText(date: Date): String {
            return SimpleDateFormat("HH:mm:ss", LOCALE_PT_BR).format(date)
        }

        val monthsMap: Map<String, Int>
            get() {
                val calendar = calendar
                return calendar.getDisplayNames(Calendar.MONTH, Calendar.ALL_STYLES, LOCALE_PT_BR)
            }

        val daysOfWeekMap: Map<String, Int>
            get() {
                val calendar = calendar
                return calendar.getDisplayNames(Calendar.DAY_OF_WEEK, Calendar.LONG, LOCALE_PT_BR)
            }

        fun getShortDayOfWeek(key: Int?): String? {
            return mapShortDaysWeek!!.get(key)
        }

        fun getDayOfMonth(date: Date): Int {
            val calendar = calendar
            calendar.time = date
            return calendar.get(Calendar.DAY_OF_MONTH)
        }

        fun getMonth(date: Date): Int {
            val calendar = calendar
            calendar.time = date
            return calendar.get(Calendar.MONTH)
        }

        fun getYear(date: Date): Int {
            val calendar = calendar
            calendar.time = date
            return calendar.get(Calendar.YEAR)
        }

        fun getMonthAndYear(date: Date): Date {
            val calendar = calendar
            calendar.time = date
            calendar.set(Calendar.DAY_OF_MONTH, 1)
            calendar.set(Calendar.HOUR, 0)
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            return calendar.time
        }

        fun getDayOfWeekAbbreviated(date: Date): String {
            val calendar = calendar
            calendar.time = date
            val dayOfWeek = calendar.getDisplayName(java.util.Calendar.DAY_OF_WEEK, Calendar.SHORT, LOCALE_PT_BR)
            return dayOfWeek.toLowerCase(LOCALE_PT_BR)
        }

        /**
         * @author c1283796 Ezequiel Messore
         * @param startDate
         * @param endDate
         * @return Retorna a quantidade de dias entre as datas.
         */
        fun compareBetweenDays(startDate: Date, endDate: Date): Long {

            val diff = endDate.time - startDate.time

            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)

        }

        /**
         * @author c1283796 Ezequiel Messore
         * @param date
         * @return String Retorna o mês com seu nome completo. Ex: janeiro.
         */
        fun getMonthFull(date: Date): String {
            val calendar = calendar
            calendar.time = date
            val dayOfWeek = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, LOCALE_PT_BR)
            return dayOfWeek.toLowerCase(LOCALE_PT_BR)
        }

        fun toDate(date: String?, format: String = "dd/MM/yyyy"): Date? {
            return try {
                val formatter = SimpleDateFormat(format)
                formatter.parse(date)
            } catch (e: Exception) {
                null
            }
        }

        fun toString(date: Date?): String {
            return if (date == null) "" else SIMPLE_DATETIME_FORMAT_PT_BR.format(date)
        }

        fun isValid(date: String): Boolean {
            var isValid: Boolean
            try {
                val df = SIMPLE_DATETIME_FORMAT_PT_BR
                df.isLenient = false
                df.parse(date)
                isValid = true
            } catch (ex: ParseException) {
                isValid = false
            }

            return isValid
        }

        fun formatStringDate(string: String): String {
            val sdf3 = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
            var d1: Date? = null
            try {
                d1 = sdf3.parse(string)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            val df = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            return df.format(d1)
        }

    }
}
