package br.com.moises.mydocs.business.document

import android.content.BroadcastReceiver
import android.content.Intent
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.Entries
import br.com.moises.mydocs.model.PreferenceKeys
import br.com.moises.mydocs.rest.document.DocumentListRestService
import kotlin.reflect.KClass

/**
 * Created by Filipe on 11/04/2018.
 */
class DocumentListBusiness() : BaseBusiness<List<Entries>>(ACTION_SUCCESS, TAG) {

    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)
        callService(PreferencesHelper.getInt(PreferenceKeys.USER_ID))
    }

    private fun callService(userId: Int) {
        val service = DocumentListRestService(userId, this)

        service.start()
    }

    override fun onRestSuccess(response: List<Entries>?) {
        super.onRestSuccess(response)
    }

    companion object {

        private val CLASS = DocumentListBusiness::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_document_list_success"


        fun makeIntent(classCaller: KClass<*>): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)

            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>) {
            BaseBusiness.registerReceiver(receiver, callerClass, ACTION_SUCCESS)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            BaseBusiness.removeReceiver(receiver)
        }
    }
}