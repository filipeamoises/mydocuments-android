package br.com.moises.mydocs.rest

/**
 * Created by Filipe on 10/04/2018.
 */
interface JsonRestReponseCallback<R> : RestReponseCallback<R> {

    fun onRestSuccess(response: R?)

}
