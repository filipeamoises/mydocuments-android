package br.com.moises.mydocs.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import br.com.moises.mydocs.model.PreferenceKeys


/**
 * Launcher activity
 *
 * @author Filipe Moises
 */
class InitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActivity()
    }

    private fun initActivity() {
        val isFirstAccess = PreferencesHelper.getBoolean(PreferenceKeys.APP_FIRST_ACCESS)
        val isLogged = PreferencesHelper.getString(PreferenceKeys.APP_SESSION_TOKEN).isNotEmpty()
        ActivityHelper.startActivity(BottomNavigationActivity::class.java)
        if (isFirstAccess) {
            ActivityHelper.startActivity(SignUpActivity::class.java)
        } else if (isLogged) {
            ActivityHelper.startActivity(BottomNavigationActivity::class.java)
        } else {
            ActivityHelper.startActivity(LoginActivity::class.java)
        }
    }
}
