package br.com.moises.mydocs.rest.login

import br.com.moises.mydocs.business.login.SignUpBusiness
import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.model.SignUp
import br.com.moises.mydocs.rest.BaseRestService

/**
 * Created by Filipe on 12/04/2018.
 */
open class SignUpRestService(private val signUp: SignUp, callback: SignUpBusiness) : BaseRestService<SignUp, Auth, UserRestApi>(callback) {

    override val apiClass: Class<UserRestApi>
        get() = UserRestApi::class.java

    override fun create(): Int = 0

    public override fun execute(userRestApi: UserRestApi) {
        super.enqueue(userRestApi.signUp(signUp))
    }

    public override fun onRestSuccess(tokenResponse: Auth?) {
        super.onRestSuccess(tokenResponse)
    }

    public override fun onRestError(response: ProblemApiResponse) {
        super.onRestError(response)
    }

    public override fun onFailure(t: Throwable) {
        super.onFailure(t)
    }
}