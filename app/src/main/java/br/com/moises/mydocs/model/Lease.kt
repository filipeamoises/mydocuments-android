package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Lease(
        @Expose
        @SerializedName("status")
        val status: String,
        @Expose
        @SerializedName("state")
        val state: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeString(state)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Lease> {
        override fun createFromParcel(parcel: Parcel): Lease {
            return Lease(parcel)
        }

        override fun newArray(size: Int): Array<Lease?> {
            return arrayOfNulls(size)
        }
    }
}