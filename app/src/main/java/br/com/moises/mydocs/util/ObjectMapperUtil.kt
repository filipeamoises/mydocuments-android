package br.com.moises.mydocs.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.lang.reflect.Modifier

/**
 * Created by Filipe on 11/04/2018.
 */
class ObjectMapperUtil private constructor() {
    companion object {

        /**
         *
         * @return
         */
        fun newUnwrappedInstance(): Gson {

            return GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.VOLATILE, Modifier.FINAL)
                    .create()
        }

        /**
         *
         * @return
         */
        fun newWrappedInstance(): Gson {

            return GsonBuilder().disableHtmlEscaping()
                    .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.VOLATILE, Modifier.FINAL)
                    .create()
        }
    }

}