package br.com.moises.mydocs.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast
import br.com.moises.mydocs.R
import br.com.moises.mydocs.adapter.BottomViewPagerAdapter
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.business.document.DocumentUploadBusiness
import br.com.moises.mydocs.fragment.ListDocumentFragment
import br.com.moises.mydocs.fragment.ProfileFragment
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.DialogHelper
import br.com.moises.mydocs.helper.FileHelper
import br.com.moises.mydocs.model.DocumentFile
import br.com.moises.mydocs.service.FileService
import kotlinx.android.synthetic.main.activity_bottom_navigation.*

class BottomNavigationActivity : SimpleActivity(R.layout.activity_bottom_navigation, R.string.app_name) {

    private var fileReceiver = FileReceiver()
    protected var isReceiverRegistered = false
    private val restReceiver = Receiver()
    lateinit var viewPagerAdapter: BottomViewPagerAdapter
    private val DOUBLE_BACK_EXIT_TIMEMILLIS = 2000
    private var mBackPressed: Long = 0
    internal lateinit var toastBackPress: Toast

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindNavigationButtons()
        toastBackPress = Toast.makeText(baseContext, R.string.msg_tap_again_to_exit, Toast.LENGTH_LONG)
    }

    override fun onBackPressed() {
        if (mBackPressed + DOUBLE_BACK_EXIT_TIMEMILLIS > SystemClock.elapsedRealtime()) {
            toastBackPress.cancel()
            finish()
        } else {
            toastBackPress.show()
        }
        mBackPressed = SystemClock.elapsedRealtime()
    }

    private fun bindNavigationButtons() {
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        // Instantiate a ViewPager and a PagerAdapter.
        viewPagerAdapter = BottomViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.add(ListDocumentFragment())
        viewPagerAdapter.add(ProfileFragment())
        vp_viewpager.adapter = viewPagerAdapter
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
        //            Documents tab
            R.id.navigation_documents -> {
                vp_viewpager.setCurrentItem(0)
                return@OnNavigationItemSelectedListener true
            }
//            Profile tab
//            R.id.navigation_profile -> {
//                vp_viewpager.setCurrentItem(1)
//                return@OnNavigationItemSelectedListener true
//            }
        }
        false
    }

    override fun onResume() {
        super.onResume()
        DocumentUploadBusiness.registerReceiver(restReceiver, this::class)
    }

    override fun onPause() {
        super.onPause()
        DocumentUploadBusiness.removeReceiver(restReceiver)
        unregisterReceiver()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            when (requestCode) {
                FileHelper.Type.IMAGE.requestCode -> {
                    Toast.makeText(ContextHelper.applicationContext, R.string.msg_file_uploading_image, Toast.LENGTH_SHORT).show()
                    callFileService(data.data)
                }
                FileHelper.Type.VIDEO.requestCode -> {
                    Toast.makeText(ContextHelper.applicationContext, R.string.msg_file_uploading_video, Toast.LENGTH_SHORT).show()
                    callFileService(data.data)
                }
                FileHelper.Type.DOC.requestCode -> {
                    Toast.makeText(ContextHelper.applicationContext, R.string.msg_file_uploading_file, Toast.LENGTH_SHORT).show()
                    callFileService(data.data)
                }
                FileHelper.Type.AUDIO.requestCode -> {
                    Toast.makeText(ContextHelper.applicationContext, R.string.msg_file_uploading_audio, Toast.LENGTH_SHORT).show()
                    callFileService(data.data)
                }
            }
        }
    }

    private fun callFileService(uri: Uri) {
        registerReceiver()
        val intent = Intent(ContextHelper.applicationContext, FileService::class.java)
        intent.putExtra(FileService.KEY_ACTION, FileService.KEY_ACTION_COPY)
        intent.putExtra(FileService.KEY_URI, uri)
        startService(intent)
    }

    private fun callUploadService(file: DocumentFile) {
        val intent = DocumentUploadBusiness.makeIntent(this::class, file.uriPath!!)
        startService(intent)
    }

    private fun registerReceiver() {
        synchronized(isReceiverRegistered) {
            if (!isReceiverRegistered) {
                LocalBroadcastManager.getInstance(this).registerReceiver(fileReceiver, FileService.makeIntentFilter())
                isReceiverRegistered = true
            }
        }
    }

    private fun unregisterReceiver() {
        synchronized(isReceiverRegistered) {
            if (isReceiverRegistered) {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(fileReceiver)
                isReceiverRegistered = false
            }
        }
    }

    private inner class Receiver : BaseBusiness.Receiver() {
        override fun onReceive(action: String, bundle: Bundle) {
            showMessage(getString(R.string.msg_file_uploaded))
        }
    }

    private inner class FileReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent!!.action) {
                FileService.RESPONSE_FILE_COPY_SUCCESSFUL -> {
                    val file = intent.getParcelableExtra<DocumentFile>(FileService.KEY_MODEL_FILE)

                    if (file != null) {
                        callUploadService(file)
                    } else {
                        dialog.type(DialogHelper.Types.ERROR)
                                .message(getString(R.string.err_file_load_error))
                                .show()
                    }
                }
                FileService.RESPONSE_FILE_DELETE_SUCCESSFUL -> {
                    // Do nothing
                }
            }
        }
    }
}