package br.com.moises.mydocs.util

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import java.io.IOException
import java.io.InputStream

/**
 * Created by Filipe on 11/04/2018.
 */
abstract class RootUnwrappedParserUtil<T> : ParserUtil<T> {

    @Throws(JsonSyntaxException::class, IOException::class)
    override fun parse(jsonContent: InputStream): T {
        throw RuntimeException("You must implement ParserUtil.parse(InputStream).")
    }

    @Throws(JsonSyntaxException::class, IOException::class)
    override fun parse(jsonContent: String): T {
        throw RuntimeException("You must implement ParserUtil.parse(String).")
    }

    @Throws(JsonSyntaxException::class, IOException::class)
    override fun toJson(t: T): ByteArray {
        throw RuntimeException("You must implement ParserUtil.toJson(T).")
    }

    fun getObjectMapper(): Gson = ObjectMapperUtil.newUnwrappedInstance()
}
