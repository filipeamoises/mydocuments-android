package br.com.moises.mydocs.util

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.content.res.AppCompatResources
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import br.com.moises.mydocs.helper.ContextHelper

/**
 * Created by Filipe on 11/04/2018.
 */
object ResUtil {

    fun getColor(@ColorRes color: Int): Int {
        return ContextCompat.getColor(ContextHelper.applicationContext, color)
    }

    fun getDrawable(@DrawableRes res: Int): Drawable? {
        return AppCompatResources.getDrawable(ContextHelper.applicationContext, res)
    }

    fun getTintedDrawable(@DrawableRes res: Int, @ColorRes color: Int, mode: PorterDuff.Mode = PorterDuff.Mode.SRC_ATOP): Drawable {
        val drawable = getDrawable(res)

        drawable!!.setColorFilter(getColor(color), mode)

        return drawable
    }

    fun tint(view: View, @ColorRes color: Int) {
        view.setBackgroundColor(getColor(color))
    }

    fun tint(view: TextView, @ColorRes color: Int) {
        view.setTextColor(getColor(color))
    }

    fun tint(view: ImageView, @ColorRes color: Int) {
        view.setColorFilter(getColor(color))
    }

    fun tint(drawable: Drawable?, @ColorRes color: Int) {
        DrawableCompat.setTint(DrawableCompat.wrap(drawable!!), getColor(color))
    }

    fun getColorHex(@ColorRes color: Int): String {
        val sb = StringBuilder(Integer.toHexString(getColor(color)))

        return sb.substring(2, sb.length)
    }
}