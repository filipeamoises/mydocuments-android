package br.com.moises.mydocs.helper

import android.media.MediaRecorder
import android.support.v7.app.AppCompatActivity
import android.util.Log
import br.com.moises.mydocs.fragment.AudioRecordFragment
import java.io.File
import java.io.IOException

/**
 * Created by Filipe on 11/04/2018.
 */
object AudioHelper {
    private val TAG = AudioHelper::class.java.canonicalName

    val AUDIO_DIR = "audio"

    private var mediaRecorder: MediaRecorder? = null

    fun recAudio(activity: AppCompatActivity, onFinished: ((File) -> Unit)? = null): Boolean {
        if (mediaRecorder != null) {
            // already recording
            return false
        }

        val filename = "Audio_${TimeHelper.getCurrentTimestamp()}.m4a"
        val file = getFile(filename)

        showFragment(activity, file, onFinished)

        val mediaRecorder = MediaRecorder()

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        mediaRecorder.setOutputFile(file.absolutePath)

        try {
            mediaRecorder.prepare()
        } catch(e: IOException) {
            Log.e(TAG, "Failed to prepare media recorder", e)
            mediaRecorder.release()
            return false
        }

        try {
            mediaRecorder.start()
        } catch (e: Exception) {
            Log.e(TAG, "Failed to start media recorder", e)
            mediaRecorder.release()
            return false
        }

        this.mediaRecorder = mediaRecorder
        return true
    }

    fun stopRecording() {
        mediaRecorder?.stop()
        mediaRecorder?.release()
        mediaRecorder = null
    }

    fun getFile(filename: String): File {
        val audioDir = File(ContextHelper.applicationContext.filesDir, AUDIO_DIR)

        if (!audioDir.exists()) {
            audioDir.mkdir()
        }

        return File(audioDir, filename)
    }

    private fun showFragment(activity: AppCompatActivity, file: File, onFinished: ((File) -> Unit)? = null) {
        AudioRecordFragment.newInstance {
            stopRecording()
            onFinished?.invoke(file)
        }.show(activity.supportFragmentManager, "AudioRecordFragment")
    }

    private fun dismissFragment(activity: AppCompatActivity) {
        val fragment = activity.supportFragmentManager.findFragmentByTag("AudioRecordFragment") as? AudioRecordFragment
        fragment?.dismiss()
    }
}
