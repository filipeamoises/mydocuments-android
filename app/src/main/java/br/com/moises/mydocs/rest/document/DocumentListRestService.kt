package br.com.moises.mydocs.rest.document

import br.com.moises.mydocs.model.Entries
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.rest.BaseRestService
import br.com.moises.mydocs.rest.JsonRestReponseCallback

/**
 * Created by Filipe on 11/04/2018.
 */
open class DocumentListRestService(private val userId: Int, callback: JsonRestReponseCallback<List<Entries>>) : BaseRestService<Long, List<Entries>, DocumentRestApi>(callback) {

    override val apiClass: Class<DocumentRestApi>
        get() = DocumentRestApi::class.java

    override fun create(): Int = userId

    public override fun execute(oauthRestApi: DocumentRestApi) {
        super.enqueue(oauthRestApi.list(create()))
    }

    public override fun onRestSuccess(tokenResponse: List<Entries>?) {
        super.onRestSuccess(tokenResponse)
    }

    public override fun onRestError(response: ProblemApiResponse) {
        super.onRestError(response)
    }

    public override fun onFailure(t: Throwable) {
        super.onFailure(t)
    }
}