package br.com.moises.mydocs

import android.app.Application
import br.com.moises.mydocs.helper.ContextHelper
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Filipe on 10/04/2018.
 */
open class MyDocsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ContextHelper.init(applicationContext)
        Fabric.with(this, Crashlytics())
    }
}