package br.com.moises.mydocs.helper

import android.content.SharedPreferences
import android.preference.PreferenceManager
import br.com.moises.mydocs.model.PreferenceKeys

/**
 * Created by Filipe on 10/04/2018.
 */
object PreferencesHelper {

    val VERSION = 0

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ContextHelper.applicationContext)

    init {
        val oldVersion = getInt(PreferenceKeys.APP_PREFERENCE_VERSION)

        if (VERSION > oldVersion) {
            onUpgrade(oldVersion, VERSION)
        } else {
            init()
        }
    }

    private fun init() {
        val keys = PreferenceKeys.values()

        keys.forEach { p ->
            if (!hasKey(p)) {
                set(p, p.defaultValue)
            }
        }
    }

    private fun onUpgrade(oldVersion: Int, newVersion: Int) {

        init()

        // Example: If new version is between 1 and 3 do reset all keys!
        /*when (newVersion) {
            in 1..3 -> {
                reset()
            }
        }*/
    }

    @Synchronized
    fun reset() {
        val editor = prefs.edit()

        editor.clear()
        editor.apply()

        init()
    }

    @Synchronized
    fun hasKey(p: PreferenceKeys): Boolean = prefs.contains(p.key)

    @Synchronized
    fun set(p: PreferenceKeys, value: Any?) {
        when (value) {
            is String -> setString(p, value)
            is Boolean -> setBoolean(p, value)
            is Int -> setInt(p, value)
            is Long -> setLong(p, value)
        }
    }

    @Synchronized
    fun setString(p: PreferenceKeys, value: String) = prefs.edit().putString(p.key, value).apply()

    @Synchronized
    fun setBoolean(p: PreferenceKeys, value: Boolean) = prefs.edit().putBoolean(p.key, value).apply()

    @Synchronized
    fun setInt(p: PreferenceKeys, value: Int) = prefs.edit().putInt(p.key, value).apply()

    @Synchronized
    fun setLong(p: PreferenceKeys, value: Long) = prefs.edit().putLong(p.key, value).apply()

    @Synchronized
    fun setStringSet(p: PreferenceKeys, value: Set<String>) = prefs.edit().putStringSet(p.key, value).apply()

    @Synchronized
    fun getString(p: PreferenceKeys): String = prefs.getString(p.key, "")

    @Synchronized
    fun getBoolean(p: PreferenceKeys): Boolean = prefs.getBoolean(p.key, true)

    @Synchronized
    fun getInt(p: PreferenceKeys): Int = prefs.getInt(p.key, -1)

    @Synchronized
    fun getLong(p: PreferenceKeys): Long = prefs.getLong(p.key, -1)

    @Synchronized
    fun getStringSet(p: PreferenceKeys): Set<String> = prefs.getStringSet(p.key, setOf())
}