package br.com.moises.mydocs.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.moises.mydocs.R
import br.com.moises.mydocs.activity.BaseActivity
import br.com.moises.mydocs.activity.InitActivity
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.helper.ActivityHelper
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.model.ProblemApiResponse
import br.com.moises.mydocs.view.FragmentView

/**
 * Created by Filipe on 10/04/2018.
 */
abstract class BaseFragment(private var layoutRes: Int) : Fragment() {

    lateinit var rootView: FragmentView
    private var receiver = Receiver()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(layoutRes, container, false)

        rootView = view as FragmentView

        return view
    }

    override fun onResume() {
        val intentFilter = BaseBusiness.buildErrorIntentFilter(this::class.java.canonicalName!!)

        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).registerReceiver(receiver, intentFilter)

        super.onResume()
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(ContextHelper.applicationContext).unregisterReceiver(receiver)

        super.onPause()
    }

    fun showMessage(message: String) {
        activity().showMessage(message)
    }

    fun showLoading() {
        rootView.showLoading()
    }

    fun hideLoading() {
        rootView.hideLoading()
    }

    fun activity() = activity as BaseActivity<*>

    open fun onRestError(response: ProblemApiResponse) {
        val msg = when {
            response.code == "401" -> {
                activity().dialog.listener {
                    showLoading()
//                    activity().startService(LogoutBusiness.makeIntent(this::class))
                    ActivityHelper.startActivityFinishingPrevious(InitActivity::class.java)
                }
                activity().dialog.title(getString(R.string.err_authentication_title))
                getString(R.string.err_authentication)
            }
            response.detail.isNullOrEmpty() -> getString(R.string.msg_local_error)
            else -> response.detail!!
        }

        hideLoading()
        activity().dialog.message(msg)
        activity().dialog.show()
    }

    open fun onRestFailure(response: String) {
        hideLoading()
        activity().dialog.message(response)
        activity().dialog.show()
    }

    inner class Receiver : BaseBusiness.Receiver() {

        override fun onReceive(action: String, bundle: Bundle) {
            when (action) {
                BaseBusiness.PROBLEM_API_RESPONSE_INTENT_ACTION -> {
                    onRestError(bundle.getParcelable(BaseBusiness.REST_ERROR_ITEM))
                }
                BaseBusiness.FAILURE_INTENT_ACTION -> onRestFailure(getString(R.string.msg_local_error))
            }
        }
    }
}