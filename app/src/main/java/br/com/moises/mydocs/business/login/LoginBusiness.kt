package br.com.moises.mydocs.business.login

import android.content.BroadcastReceiver
import android.content.Intent
import br.com.moises.mydocs.business.BaseBusiness
import br.com.moises.mydocs.model.Auth
import br.com.moises.mydocs.model.Login
import br.com.moises.mydocs.rest.login.LoginRestService
import kotlin.reflect.KClass

/**
 * Created by Filipe on 12/04/2018.
 */
class LoginBusiness : BaseBusiness<Auth>(ACTION_SUCCESS, TAG) {
    override fun onHandleIntent(intent: Intent?) {
        super.onHandleIntent(intent)

        val login = intent!!.getParcelableExtra<Login>(KEY_LOGIN)

        callService(login)
    }

    private fun callService(login: Login) {
        val service = LoginRestService(login, this)

        service.start()
    }

    override fun onRestSuccess(response: Auth?) {
        super.onRestSuccess(response)
    }

    companion object {

        private val CLASS = LoginBusiness::class

        private val TAG = CLASS.java.canonicalName

        private val ACTION_SUCCESS: String = "action_login_success"

        private val KEY_LOGIN = "key_login"

        fun makeIntent(classCaller: KClass<*>, login: Login): Intent {
            val intent = BaseBusiness.makeIntent(classCaller, CLASS)
            intent.putExtra(KEY_LOGIN, login)
            return intent
        }

        fun registerReceiver(receiver: BroadcastReceiver, callerClass: KClass<*>) {
            BaseBusiness.registerReceiver(receiver, callerClass, ACTION_SUCCESS)
        }

        fun removeReceiver(receiver: BroadcastReceiver) {
            BaseBusiness.removeReceiver(receiver)
        }
    }
}