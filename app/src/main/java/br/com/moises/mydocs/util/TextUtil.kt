package br.com.moises.mydocs.util

import android.util.Patterns
import java.text.Normalizer

/**
 * Created by Filipe on 12/04/2018.
 */
object TextUtil {

    fun validateCpf(cpf: String) = CPF.validade(cpf)

    fun validateEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()


    /**
     * CPF validator
     *
     * @author Marcelo Sampaio
     */
    object CPF {

        fun validade(cpf: String): Boolean {
            if (cpf.isEmpty() || isFake(cpf)) {
                return false
            }

            var cpfClean = cpf.replace("\\.", "")

            cpfClean = cpfClean.replace("-", "")

            var i = 0
            var firstSum = 0
            var secondSum = 0
            val firstDigit: Int
            val secondDigit: Int
            var firstDigitCheck = 0
            var secondDigitCheck = 0

            for (character in cpfClean) {
                val value = character.toString().toInt()

                // step 1
                if (i <= 8) {
                    firstSum += value * (10 - i)
                }

                // step 2
                if (i <= 9) {
                    secondSum += value * (11 - i)
                }

                //
                if (i == 9) {
                    firstDigitCheck = value
                } else if (i == 10) {
                    secondDigitCheck = value
                }

                // digit control
                i += 1
            }

            // with firstSum I'll get firstDigit
            if (firstSum % 11 < 2) {
                firstDigit = 0
            } else {
                firstDigit = 11 - (firstSum % 11)
            }

            // with secondSum I'll get secondDigit
            if (secondSum % 11 < 2) {
                secondDigit = 0
            } else {
                secondDigit = 11 - (secondSum % 11)
            }

            // validate digits
            return (firstDigit == firstDigitCheck) && (secondDigit == secondDigitCheck)
        }

        fun isFake(cpf: String): Boolean {
            return (cpf == "00000000000" ||
                    cpf == "11111111111" ||
                    cpf == "22222222222" ||
                    cpf == "33333333333" ||
                    cpf == "44444444444" ||
                    cpf == "55555555555" ||
                    cpf == "66666666666" ||
                    cpf == "77777777777" ||
                    cpf == "88888888888" ||
                    cpf == "99999999999")
        }
    }

    fun removeDiacriticalMarks(text: String) =
            Normalizer.normalize(text, Normalizer.Form.NFD)
                    .replace(Regex("\\p{M}"), "")
}