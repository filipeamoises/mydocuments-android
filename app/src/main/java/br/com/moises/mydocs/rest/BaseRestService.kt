package br.com.moises.mydocs.rest

import br.com.moises.mydocs.helper.ContextHelper

/**
 * Created by Filipe on 11/04/2018.
 */
abstract class BaseRestService<R, C, A>(private val restReponseCallback: RestReponseCallback<C>?): RestService<R, C, A>(restReponseCallback)   {

    override fun getDomainPath(): String {
        return ContextHelper.getHttpHost()
    }
}