package br.com.moises.mydocs.dao.image

import android.content.ContentValues
import android.database.Cursor
import br.com.moises.mydocs.dao.BaseContract
import br.com.moises.mydocs.dao.BaseDAO
import br.com.moises.mydocs.dao.ColumnValue
import br.com.moises.mydocs.dao.Where

/**
 * Created by Filipe on 10/04/2018.
 */
internal class ImageDAO : BaseDAO<ImageModel>() {

    override val contract = ImageContract()

    @Synchronized
    fun hasCachedFiles(): Boolean {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.ID,
                value = BaseContract.NULL,
                compare = Where.Compare.IS))
        val where = Where(args)

        return select(where).isNotEmpty()
    }


    @Synchronized
    fun hasFile(fileUri: String): Boolean {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.URI,
                value = fileUri))
        val where = Where(args)

        return select(where).isNotEmpty()
    }

    @Synchronized
    fun find(fileUri: String): ImageModel? {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.URI,
                value = "\"$fileUri\""))
        val where = Where(args)
        val result = select(where)

        return if (result.isNotEmpty()) result[0] else null
    }

    @Synchronized
    fun findImageByUserId(userId: Long): List<ImageModel> {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.USER_ID,
                value = userId.toString()))
        val where = Where(args)

        return select(where)
    }

    @Synchronized
    fun setUploaded(fileId: Long) {
        val columnValue = ColumnValue(
                column = ImageContract.Columns.UPLOADED,
                value = BaseContract.TRUE)

        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.ID,
                value = fileId.toString()))

        val where = Where(args)

        return update(columsValues = columnValue, where = where)
    }

    @Synchronized
    fun delete(fileUri: String) {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.URI,
                value = fileUri))
        val where = Where(args)

        return delete(where)
    }

    @Synchronized
    fun deleteAllByUserId(manifestationId: Long) {
        val args = arrayListOf(Where.Arg(
                column = ImageContract.Columns.USER_ID,
                value = manifestationId.toString()))
        val where = Where(args)

        return delete(where)
    }

    @Synchronized
    fun save(file: ImageModel) {
        val model = find(file.uri!!)

        if (model != null) {
            val idValue = ColumnValue(ImageContract.Columns.ID, file.id?.toString() ?: "-1")
            val manifestationIdValue = ColumnValue(ImageContract.Columns.USER_ID, file.userId?.toString() ?: "-1")
            val uriValue = ColumnValue(ImageContract.Columns.URI, file.uri ?: "-1")
            val hashValue = ColumnValue(ImageContract.Columns.HASH, file.hash ?: "")
            val uploadedValue = ColumnValue(ImageContract.Columns.UPLOADED, if (file.uploaded == true) "0" else "1")

            val args = arrayListOf(Where.Arg(column = "id", compare = Where.Compare.IS, value = file.id.toString()))
            val where = Where(args)

            update(idValue, manifestationIdValue, uriValue, hashValue, uploadedValue, where = where)
        } else {
            insert(file)
        }
    }

    @Synchronized
    override fun toModel(cursor: Cursor): ImageModel {
        val model = ImageModel()

        model.id = getLong(cursor, ImageContract.Columns.ID)
        model.uri = getString(cursor, ImageContract.Columns.URI)
        model.hash = getString(cursor, ImageContract.Columns.HASH)
        model.uploaded = getInt(cursor, ImageContract.Columns.UPLOADED) != 0

        return model
    }

    @Synchronized
    override fun toContentValues(model: ImageModel): ContentValues {
        val values = ContentValues()

        values.put(ImageContract.Columns.ID, model.id)
        values.put(ImageContract.Columns.USER_ID, model.userId)
        values.put(ImageContract.Columns.URI, model.uri)
        values.put(ImageContract.Columns.HASH, model.hash)
        values.put(ImageContract.Columns.UPLOADED, if (model.uploaded == true) 1 else 0)

        return values
    }

    companion object {

        val instance = ImageDAO()
    }
}