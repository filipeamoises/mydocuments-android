package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Filipe on 12/04/2018.
 */
open class SignUp(

        var name: String? = null,
        var email: String? = null,
        var password: String? = null,
        var phone: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(password)
        parcel.writeString(phone)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<SignUp> {

        override fun createFromParcel(parcel: Parcel): SignUp = SignUp(parcel)

        override fun newArray(size: Int): Array<SignUp?> = arrayOfNulls(size)
    }
}
