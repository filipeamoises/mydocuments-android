package br.com.moises.mydocs.model

import android.os.Parcel
import android.os.Parcelable
import br.com.moises.mydocs.R
import br.com.moises.mydocs.helper.ContextHelper
import br.com.moises.mydocs.helper.PreferencesHelper
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class Entries(
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("lastModified")
        val lastmodified: String,
        @Expose
        @SerializedName("etag")
        val etag: String,
        @Expose
        @SerializedName("contentLength")
        val contentlength: String,
        @Expose
        @SerializedName("contentSettings")
        var contentsettings: ContentSettings,
        @Expose
        @SerializedName("blobType")
        val blobtype: String,
        @Expose
        @SerializedName("lease")
        var lease: Lease,
        @Expose
        @SerializedName("serverEncrypted")
        val serverencrypted: String,
        var localStore: Boolean = false): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(ContentSettings::class.java.classLoader),
            parcel.readString(),
            parcel.readParcelable(Lease::class.java.classLoader),
            parcel.readString(),
            parcel.readValue(Long::class.java.classLoader) as Boolean) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(lastmodified)
        parcel.writeString(etag)
        parcel.writeString(contentlength)
        parcel.writeParcelable(contentsettings, flags)
        parcel.writeString(blobtype)
        parcel.writeParcelable(lease, flags)
        parcel.writeString(serverencrypted)
        this.localStore?.let { parcel.writeValue(it) }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Entries> {
        override fun createFromParcel(parcel: Parcel): Entries {
            return Entries(parcel)
        }

        override fun newArray(size: Int): Array<Entries?> {
            return arrayOfNulls(size)
        }
    }

    fun getFileUrl() : String{
        return ContextHelper.applicationContext.getString(R.string.url_storage) + "container"+ PreferencesHelper.getInt(PreferenceKeys.USER_ID) + "/" + name
    }


}
