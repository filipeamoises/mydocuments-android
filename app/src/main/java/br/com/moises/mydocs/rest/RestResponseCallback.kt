package br.com.moises.mydocs.rest

import br.com.moises.mydocs.model.ProblemApiResponse

/**
 * Created by Filipe on 10/04/2018.
 */
interface RestReponseCallback<R> {

    fun onRestError(response: ProblemApiResponse)

    fun onFailure(t: Throwable)


}
