package br.com.moises.mydocs.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import br.com.moises.mydocs.R
import kotlinx.android.synthetic.main.fragment_record_audio.*

/**
 * Created by Filipe on 11/04/2018.
 */
class AudioRecordFragment: DialogFragment() {
    companion object {
        // Factory Method
        fun newInstance(onStoppedRecording: () -> Unit): AudioRecordFragment {
            val f = AudioRecordFragment()
            // NOTE: this might cause problem on configuration change (orientation, for example)
            f.onStoppedRecording = onStoppedRecording

            // Supply num input as an argument.
            val args = Bundle()
            f.arguments = args

            return f
        }
    }

    private lateinit var onStoppedRecording: () -> Unit
    private var userStoppedRecording = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        // Remove Title bar on API 19
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)

        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        isCancelable = false
        dialog.setCanceledOnTouchOutside(false)
        // can inflater ever be null?
        return inflater!!.inflate(R.layout.fragment_record_audio, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews()
    }

    private fun bindViews() {
        bt_stop.setOnClickListener {
            userStoppedRecording = true
            onStoppedRecording()

            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        if (userStoppedRecording) {
            super.onDismiss(dialog)
        }
    }
}
