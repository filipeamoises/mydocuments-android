package br.com.moises.mydocs.view

import android.content.Context
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.AppCompatEditText
import android.text.InputFilter
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import br.com.moises.mydocs.R

/**
 * Created by Filipe on 12/04/2018.
 */
class InputField : FrameLayout {

    lateinit var inputLayout: TextInputLayout
        private set

    lateinit var editText: AppCompatEditText
        private set

    private var isFinishInflate: Boolean = false

    var text: String = ""
        get() {
            field = editText.text.toString()
            return field
        }
        set(value) {
            field = value
            editText.setText(value)
        }

    var error: String = ""
        get() {
            field = editText.error.toString()
            return field
        }
        set(value) {
            field = value
            editText.error = value
        }

    var hint: String = ""
        get() {
            field = inputLayout.hint.toString()
            return field
        }
        set(value) {
            field = value
            inputLayout.hint = value
        }

    var input: Input = Input.NORMAL
        set(value) {
            field = value

            editText.setSingleLine(true)

            when (value) {
                Input.NORMAL -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                }
                Input.NAME -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                }
                Input.EMAIL -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                }
                Input.PASSWORD -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT
                    editText.transformationMethod = PasswordTransformationMethod.getInstance()
                }
                Input.AREA -> {
                    editText.inputType = InputType.TYPE_CLASS_TEXT
                    editText.setSingleLine(false)
                }
                Input.PHONE -> {
                    editText.inputType = InputType.TYPE_CLASS_PHONE
                    editText.setSingleLine(true)
                }
            }
        }

    constructor(context: Context) : super(context) {
        config(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        config(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        config(attrs)
    }

    private fun config(attrs: AttributeSet?) {
        if (!isInEditMode) {
            LayoutInflater.from(context).inflate(R.layout.view_input_field, this, true)

            inputLayout = findViewById(R.id.input_layout)
            editText = findViewById(R.id.field)

            val a = context.theme.obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomView,
                    0, 0)

            try {
                // Hint
                if (a.hasValue(R.styleable.CustomView_custom_hint))
                    hint = a.getString(R.styleable.CustomView_custom_hint)

                // Input type
                if (a.hasValue(R.styleable.CustomView_custom_input)) {
                    val index = a.getInt(R.styleable.CustomView_custom_input, 0)

                    input = when (index) {
                        Input.NAME.v -> Input.NAME
                        Input.EMAIL.v -> Input.EMAIL
                        Input.PASSWORD.v -> Input.PASSWORD
                        Input.AREA.v -> Input.AREA
                        Input.PHONE.v -> Input.PHONE
                        else -> Input.NORMAL
                    }
                } else {
                    input = Input.NORMAL
                }

                // Is last field
                if (a.hasValue(R.styleable.CustomView_custom_last_field)) {
                    val isLastField = a.getBoolean(R.styleable.CustomView_custom_last_field, false)

                    if (isLastField) {
                        editText.imeOptions = EditorInfo.IME_ACTION_DONE
                    } else {
                        editText.imeOptions = EditorInfo.IME_ACTION_NEXT
                    }
                } else {
                    editText.imeOptions = EditorInfo.IME_ACTION_NEXT
                }

                // Set text Color
                if (a.hasValue(R.styleable.CustomView_custom_textColor)) {
                    editText.setTextColor(a.getInt(R.styleable.CustomView_custom_textColor, R.color.colorPrimaryDark))

                }

                // Set text length
                if (a.hasValue(R.styleable.CustomView_custom_textMaxLength)) {
                    editText.filters = arrayOf(InputFilter.LengthFilter(a.getInt(R.styleable.CustomView_custom_textMaxLength, 500)))
                }
            } finally {
                a.recycle()
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        isFinishInflate = true
    }

    enum class Input(val n: String, val v: Int) {
        NORMAL("normal", 0),
        NAME("name", 1),
        EMAIL("email", 2),
        PASSWORD("password", 3),
        AREA("area", 4),
        PHONE("phone",5)
    }
}