package br.com.moises.mydocs.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import br.com.moises.mydocs.R

/**
 * Created by Filipe on 10/04/2018.
 */
open class FragmentView : ConstraintLayout {

    lateinit var contentView: ConstraintLayout

    lateinit var loadingView: ConstraintLayout

    private var layoutInflated: Boolean = false

    private var isFinishInflate: Boolean = false

    protected var isLoading: Boolean = false
        set(value) {
            field = value
            loadingView.visibility = if (value) View.VISIBLE else View.GONE
            contentView.visibility = if (value) View.GONE else View.VISIBLE
        }

    constructor(context: Context) : super(context) {
        config()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        config()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        config()
    }

    protected fun config() {
        if (!isInEditMode) {
            LayoutInflater.from(context).inflate(R.layout.view_fragment, this, true)

            loadingView = findViewById(R.id.fragment_loading)
            contentView = findViewById(R.id.fragment_content)

            hideLoading()
        }
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (layoutInflated) {
            contentView.addView(child, index, params)
        } else {
            super.addView(child, index, params)
            layoutInflated = true
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        isFinishInflate = true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean = isLoading

    fun showLoading() {
        isLoading = true
    }

    fun hideLoading() {
        isLoading = false
    }
}